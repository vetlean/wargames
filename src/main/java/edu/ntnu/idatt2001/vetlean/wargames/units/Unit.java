package edu.ntnu.idatt2001.vetlean.wargames.units;

import java.io.Serializable;
import java.util.Objects;
import java.util.Random;

/**
 * this is the abstract class of all units. every unit inherits this class.
 * it defines the similarities that all units has
 */

public abstract class Unit implements Serializable{

    private String name;
    private int health;
    private int attack;
    private int armor;
    private final double accuracy;
    private final double counterattack;
    private static final Random random = new Random();

    /**
     * the constructor of the unit class. it checks if all the parameters is valid.
     * name can not be empty.
     * health has to be smaller than 0
     * attack can not be smaller than 0
     * armor can not be smaller than 0
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     * @param accuracy a double representing the percentage of hits
     * @param counterattack a double representing the percentage of time the unit counter an attack
     */

    public Unit(String name, int health, int attack, int armor, double accuracy, double counterattack) {
        if (name.isBlank()){
            throw new IllegalArgumentException("The unit must have a name, i can not be black");
        }
        this.name = name;
        if (health<=-1){
            throw new IllegalArgumentException("The unit can not have a negative health value");
        }
        this.health = health;
        if (attack<=-1){
            throw new IllegalArgumentException("The unit can not have a negative attack value");
        }
        this.attack = attack;
        if (armor<=-1){
            throw new IllegalArgumentException("The unit can not have a negative armor value");
        }
        this.armor = armor;
        if (accuracy < 0 || accuracy > 1){
            throw new IllegalArgumentException("The unit's accuracy cannot be less than 25 percent or greater than 100 percent");
        }
        this.accuracy = accuracy;
        if (counterattack < 0 || counterattack > 1){
            throw new IllegalArgumentException("The unit's counterattack cannot be less than 25 percent or greater than 100 percent");
        }
        this.counterattack = counterattack;
    }

    /**
     * @return string, returns the name of the unit
     */
    public String getName() {
        return name;
    }

    /**
     * @return integer the health of the unit
     */
    public int getHealth() {
        return health;
    }

    /**
     * sets the health of the unit, it can not be smaller than zero.
     * @param health an ineger represnting the healt of the unit
     */
    public void setHealth(int health) {
        if(health<0){
            throw new IllegalArgumentException("can not set health to value less than zero");
        }
        this.health = health;
    }


    /**
     * sets the name of the unit
     * @param name an string of the new name
     */
    public void setName(String name) throws IllegalArgumentException {
        if (name.isBlank()){
            throw new IllegalArgumentException("The unit must have a name, i can not be black");
        }
        this.name = name;
    }

    /**
     * @return integer, representing the attack damage a unit can give
     */
    public int getAttack() {
        return attack;
    }

    /**
     * sets tha attack value of the unit
     * @param attack an in representing the attack value.
     */
    public void setAttack(int attack){
        if (attack<=-1){
            throw new IllegalArgumentException("The unit can not have a negative attack value");
        }
        this.attack = attack;
    }

    /**
     * @return integer, representing the damage the armor can take
     */
    public int getArmor() {
        return armor;
    }

    /**
     * sets the armor value of the unit
     * @param armor an integer representing the armor value.
     */
    public void setArmor(int armor){
        if (armor<=-1){
            throw new IllegalArgumentException("The unit can not have a negative armor value");
        }
        this.armor = armor;
    }

    /**
     * @return integer, representing the attack bonus the unit has
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * @return integer, representing the armor bonus the unit has
     */
    public abstract int getResistBonus(Terrain terrain);

    /**
     * @return a boolean representing if the unit hits the attck
     */
    public boolean hit(){
        return random.nextDouble() < accuracy;
    }

    /**
     * @return a boolean representing if the unit counters the attack.
     */
    public boolean opponentCounterattack(){
        return random.nextDouble() < counterattack;
    }

    /**
     * the logical algorithm when a unit attack another unit.
     * If the opponent counterattack is triggered the defending unit attacks in sted.
     * If the attacking unit misses the defending unit does not lose any health.
     * @param opponent the unit that's being attacked.
     * @param terrain representing the terrain the units are fighting on.
     */
    public void attack(Unit opponent, Terrain terrain){
        int opponentHealth = opponent.getHealth();
        int damage = this.attack + this.getAttackBonus(terrain);
        int armor = opponent.getArmor() + opponent.getResistBonus(terrain);

        if (opponent.opponentCounterattack()){
            opponent.attack(this, terrain);
       }
        if(!hit()){
            opponent.setHealth(opponentHealth);
        }else if (opponentHealth - damage +armor <= 0){
            opponent.setHealth(0);
        }
        else if(armor<damage){
            opponent.setHealth( opponentHealth - damage + armor);
        }
    }

    /**
     * this method does not take in the paramter of accuracy or counterattack
     * This method is for testing,
     * it is useful for test when attack hits it deals the damage that is expected.
     * The other method is hard to test because it is random if the attack hits or get countered.
     * @param opponent the unit that's being attacked.
     * @param terrain representing the terrain the units are fighting on.n
     */
    public void testAttack(Unit opponent, Terrain terrain){
        int opponentHealth = opponent.getHealth();
        int damage = this.attack + this.getAttackBonus(terrain);
        int armor = opponent.getArmor() + opponent.getResistBonus(terrain);

        if (opponentHealth - damage +armor <= 0){
            opponent.setHealth(0);
        }
        else if(armor<damage){
            opponent.setHealth( opponentHealth - damage + armor);
        }
    }


    /**
     * @return a string representation of the unit.
     */
    @Override
    public String toString() {
        return "Unit " +
                " name =" + name +
                " health = " + health +
                " attack = " + attack +
                " armor = " + armor;
    }

    /**
     * This method is practical for testing. not used int the function of the program.
     * Used under testing of sorting.
     * practical because when comparing two List with units wit assertEquals
     * it uses this equals method to check if tow units ar equal.
     * @param o the unit being tested to ceheck if it is equal.
     * @return a boolean, true if they are equal false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return health == unit.health && attack == unit.attack && armor == unit.armor && Objects.equals(name, unit.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, health, attack, armor);
    }
}
