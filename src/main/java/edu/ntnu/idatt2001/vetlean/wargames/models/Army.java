package edu.ntnu.idatt2001.vetlean.wargames.models;
/**
 * This is the Army class. Here all the units is collected to make an army.
 *
 */



import edu.ntnu.idatt2001.vetlean.wargames.units.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Army implements Serializable {

    private String name;
    private List<Unit> units;
    private final Random random;

    /**
     * This is the simple constructor for an army. you only need a name, and it will make an empty array list
     * @param name a string with the name of army.
     */
    public Army(String name) {
        setName(name);
        this.units = new ArrayList<>();
        random = new Random();
    }


    /**
     * this is the constructor of an army, it takes a name and an arraylist with units.
     * @param name a string with the name of army.
     * @param units a List holding the units of the army.
     */
    public Army(String name, List<Unit> units) {
        setName(name);
        this.units = units;
        random = new Random();
    }

    /**
     * adds one unit to Army
     * @param unit the unit being added
     * @throws NullPointerException if the unit give is null.
     */
    public void add(Unit unit){
        if (unit == null){
            throw new NullPointerException("The object you try to add is empty(null)");
        }
        units.add(unit);
    }

    /**
     * takes a list of units and adds the units to the existing units.
     * @param units the List of units being added.
     */
    public void addAll(List<Unit> units){
        if (Objects.isNull(units)){
            throw new IllegalArgumentException("the list of units trying to add is null");
        }
        this.units.addAll(units);
    }

    /**
     * removes one unit from the army, often used to remove one dead unit.
     * it checks if the unit has a value, if null throws a null point exception
     * @param unit the unit being removed
     */
    public void remove(Unit unit){
        if (unit == null){
            throw new NullPointerException("The object you try to remove is empty(null)");
        }
        units.remove(unit);

    }

    /**
     * cheks if the list Units in army is empty
     * @return boolean, true if arraylist has units. false if empty.
     */
    public boolean hasUnits(){
        return !units.isEmpty();
    }

    /**
     * @return the name of the Army
     */
    public String getName() {
        return name;
    }

    /**
     * changes the name of the army
     * @param name a string with the new name of the army
     */
    public void setName(String name) {
        if(name.isEmpty()){
            throw new IllegalArgumentException("The name of the army can not be empty");
        }
        this.name = name;
    }

    /**
     * @return the list Units from army
     */
    public List<Unit> getUnits() {
        return units;
    }

    /**
     * Sorts the Army for the remaining unit of class InfantryUnits
     * @return a List containing only object of class InfantryUnit
     */
    public List<Unit> getInfantryUnits(){
        return units.stream().filter((unit -> unit.getClass() == InfantryUnit.class)).collect(Collectors.toList());
    }

    /**
     * Sorts the Army for the remaining unit of class CavalryUnit
     * @return a List containing only object of class CavalryUnit
     */
    public List<Unit> getCavalryUnits(){
        return units.stream().filter((unit -> unit.getClass() == CavalryUnit.class)).collect(Collectors.toList());
    }

    /**
     * Sorts the Army for the remaining unit of class RangedUnits
     * @return a List containing only object of class RangedUnits
     */
    public List<Unit> getRangedUnits(){
        return units.stream().filter((unit -> unit.getClass() == RangedUnit.class)).collect(Collectors.toList());
    }

    /**
     * Sorts the Army for the remaining unit of class CommanderUnit
     * @return a List containing only object of class CommanderUnit
     */
    public List<Unit> getCommanderUnits(){
        return units.stream().filter((unit -> unit.getClass() == CommanderUnit.class)).collect(Collectors.toList());
    }


    /**
     * finds one random unit from arraylist units
     * @return a random unit form the army
     */
    public Unit getRandom(){
        if(!hasUnits()){
            throw new IndexOutOfBoundsException("The List of units is empty");
        }
        return units.get(random.nextInt(units.size()));
    }

    /**
     * make object in to string
     * @return a string that represents the army
     */
    @Override
    public String toString() {
        return "Army{" +
                "name='" + name + '\'' +
                ", units=" + units.size() +
                '}';
    }

    /**
     * checks if one object is similar to the other.
     * @param o is the object we check if  it is equal.
     * @return boolen, true if the object are equal false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units.size(), army.units.size());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, units.size());
    }
}
