package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;


import java.io.File;

/**
 * a class that hold methods that the many controllers needs
 */
public class Util extends BaseController{

    /**
     * makes a sting representation of an army
     * @param army the army you need to represent
     * @return a sting representing the army
     */

    public static  String representationOfArmy(Army army){

        StringBuilder stringBuilder = new StringBuilder();

        String nameOfArmy = "The name of the army is: " + army.getName() + "\n";
        String numberOfUnits = "Total units: " + army.getUnits().size() + "\n";
        String numberOfCommander = "Total Commander units: " + army.getCommanderUnits().size() + "\n";
        String numberOfCavalry = "Total Cavalry units: " + army.getCavalryUnits().size() + "\n";
        String numberOfInfrantry = "Total Infantry units: " + army.getInfantryUnits().size() + "\n";
        String numberOfRanged = "Total Ranged units: " + army.getRangedUnits().size() + "\n";

        stringBuilder.append(nameOfArmy);
        stringBuilder.append(numberOfUnits);
        stringBuilder.append(numberOfCommander);
        stringBuilder.append(numberOfCavalry);
        stringBuilder.append(numberOfInfrantry);
        stringBuilder.append(numberOfRanged);

        return stringBuilder.toString();
    }

    /**
     * makes a sting representation of an army
     * with stats of each unit
     * @param army the army you need to represent
     * @return a sting representing the army
     */
    public static  String representationOfArmyWithStats(Army army){

        StringBuilder stringBuilder = new StringBuilder();

        String nameOfArmy = "The name of the army is: " + army.getName() + "\n";
        String numberOfUnits = "Total units: " + army.getUnits().size() + "\n";
        String numberOfCommander = "Total Commander units: " + army.getCommanderUnits().size() + "\n";
        String numberOfCavalry = "Total Cavalry units: " + army.getCavalryUnits().size() + "\n";
        String numberOfInfrantry = "Total Infantry units: " + army.getInfantryUnits().size() + "\n";
        String numberOfRanged = "Total Ranged units: " + army.getRangedUnits().size() + "\n";

        stringBuilder.append(nameOfArmy);
        stringBuilder.append(numberOfUnits);
        stringBuilder.append(numberOfCommander);
        if (army.getCommanderUnits().size() > 0){
            Unit commander = army.getCommanderUnits().get(0);
            String commanderUnitStats = "Commander Uit = Name "+ commander.getName()+ ", Health: " + commander.getHealth() + ", Attack: "
                    + commander.getAttack() + ", Armor: " + commander.getArmor() + "\n";
            stringBuilder.append(commanderUnitStats);
        }
        stringBuilder.append(numberOfCavalry);
        if (army.getCavalryUnits().size() > 0){
            Unit cavalry = army.getCavalryUnits().get(0);
            String cavalryUnitStats = "Cavalry Uit = Name "+ cavalry.getName()+ ", Health: " + cavalry.getHealth() + ", Attack: "
                    + cavalry.getAttack() + ", Armor: " + cavalry.getArmor() + "\n";
            stringBuilder.append(cavalryUnitStats);
        }
        stringBuilder.append(numberOfInfrantry);
        if (army.getInfantryUnits().size() > 0){
            Unit infantry = army.getInfantryUnits().get(0);
            String infantryUnitStats = "Infantry Uit = Name "+ infantry.getName()+ ", Health: " + infantry.getHealth() + ", Attack: "
                    + infantry.getAttack() + ", Armor: " + infantry.getArmor() + "\n";
            stringBuilder.append(infantryUnitStats);
        }
        stringBuilder.append(numberOfRanged);
        if (army.getRangedUnits().size() > 0){
            Unit ranged = army.getRangedUnits().get(0);
            String infantryUnitStats = "Ranged Uit = Name: "+ ranged.getName()+ ", Health: " + ranged.getHealth() + ", Attack: "
                    + ranged.getAttack() + ", Armor: " + ranged.getArmor() + "\n";
            stringBuilder.append(infantryUnitStats);
        }



        return stringBuilder.toString();
    }

    /**
     * saves the army
     * can only save one army at the time
     * uses the writeArmy method to save army
     * open the file explorer so the user can choose where to save the army
     * uses a FileChooser for this function
     * @return a file with the correct location
     */
    public static File saveArmyToFile(TextField saveArmyFiled){

        FileChooser fileChooser = new FileChooser();

        // Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Army Files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        //sets the name of the file to the one form the Interface
        fileChooser.setInitialFileName(saveArmyFiled.getText());

        //opens the file explorer in the resource folder differentArmies
        String pathToResource = "src/main/resources/edu/ntnu/idatt2001/vetlean/wargames/differnetaArmies";
        File resorseFolder = new File(pathToResource);
        fileChooser.setInitialDirectory(resorseFolder);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(view.getStage());

        if (file == null) {
            throw new NullPointerException("the file was never initialized");
        }

        return file;
    }


}
