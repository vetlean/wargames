package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.filehandler.ArmyFileHandler;
import edu.ntnu.idatt2001.vetlean.wargames.Popups;
import edu.ntnu.idatt2001.vetlean.wargames.View;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * The controller that controls the scene read-army-file-view
 * The class controls the information from the user regarding loading an army from a file.
 */
public class ReadArmyFileController extends BaseController{

    @FXML
    private TextArea armyOneTextArea, armyTwoTextArea;
    @FXML
    private RadioButton radioButtonArmyOne,radioButtonArmyTwo;
    @FXML
    private ScrollPane scrollPane;

    /**
     * This fills the inforation fields to the interface.
     */
    public void initialize(){
        fillArmyFields(modelBattle.getArmyOne());
        fillArmyFields(modelBattle.getArmyTwo());
        fillScrollPane();
    }

    /**
     * switches the scene to MAKE_ARMY_VIEW
     * @param event the event of press the button
     */
    public void switchToSceneMakeArmy(ActionEvent event){
        view.setCurrentScene(View.MAKE_ARMY_VIEW);
    }

    /**
     * switches the scene to BATTEL_SIMULATION_VIEW
     * @param event the event of press the button
     */
    public void switchToBattelView(ActionEvent event) {
        view.setCurrentScene(View.BATTEL_SIMULATION_VIEW);
    }

    /**
     * switches the scene to OPENING_VIEW
     * @param event the event of press the button
     */
    public void switchToOpeningView(ActionEvent event) {
        view.setCurrentScene(View.OPENING_VIEW);
    }

    /**
     * opens the file explorer.
     * this way the user can find the file/army he wants to use
     * @return an army
     */
    public Army readFile() throws IOException {

        FileChooser fileChooser = new FileChooser();
        Army army = null;

        // Show save file dialog
        File file = fileChooser.showOpenDialog(view.getStage());


        army = armyFileHandler.readArmy(file);


        if (file == null) {
            throw new NullPointerException("the file was never initialized");
        }
        return army;

    }

    /**
     * fills the army field on the given window
     * gives information on what the armies contains
     * @param army the army you want to represent
     */
    private void fillArmyFields(Army army){

        String armyAsString = Util.representationOfArmyWithStats(army);

        if (army == modelBattle.getArmyOne()){
            armyOneTextArea.setText(armyAsString);
        }else {
            armyTwoTextArea.setText(armyAsString);
        }

    }

    /**
     * fills the scrollPane with grouped radio buttons
     * these radio buttons is "linked" to the file with corresponding name.
     */
    public void fillScrollPane(){
        String style = "-fx-background-color: #116466";

        VBox vBox = new VBox();
        vBox.setStyle(style);

        final ToggleGroup groupArmies = new ToggleGroup();

        String path = "src/main/resources/edu/ntnu/idatt2001/vetlean/wargames/differnetaArmies";
        File dir = new File(path);

        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                RadioButton radioButton = new RadioButton(child.getName());
                radioButton.setToggleGroup(groupArmies);
                vBox.getChildren().add(radioButton);
            }
        }

        scrollPane.setContent(vBox);
    }

    /**
     * Loops through the radio buttons and reads the army it is "linked" to.
     */
    public void addUnitsFromApplication() {

        try {
            if (!radioButtonArmyOne.isSelected() && !radioButtonArmyTwo.isSelected()) {
                throw new IllegalArgumentException("you have not selected any army to add unit(s)");
            }
        }catch (IllegalArgumentException e){
            Popups.showError("loading error", "You have not selected an Army",e.getMessage());
        }

        VBox vBox = null;


        if (scrollPane.getContent() instanceof VBox){
            vBox = (VBox) scrollPane.getContent();
        }

        try {

            assert vBox != null;
            vBox.getChildren().forEach((node)-> {

                RadioButton radioButton = null;
                if (node instanceof RadioButton){
                    radioButton = (RadioButton) node;
                }
                if (Objects.isNull(radioButton)){
                    throw new IllegalArgumentException("The node in the vBox is not a radioButton, it is " + node.getClass());
                }

                if (radioButton.isSelected()){
                    ArmyFileHandler armyFileHandler = new ArmyFileHandler();
                    String path = "src/main/resources/edu/ntnu/idatt2001/vetlean/wargames/differnetaArmies/" + radioButton.getText();
                    File file = new File(path);
                    Army army = null;

                    try {
                        army = armyFileHandler.readArmy(file);
                    } catch (IOException e) {
                        throw new IllegalArgumentException("could not find the file in the resource folder, or the path to the resource folder is incorrect.");
                    }

                    addUnits(army);

                }
            });
        }catch (Exception e){
            Popups.showError("Loading error","Could not read file", e.getMessage());
        }
    }

    /**
     * add units to the army that is selected
     * @param event the press of the load selected armies
     */
    public void addUnitsWithFileExplorer(ActionEvent event){

        try {

            if (!radioButtonArmyOne.isSelected() && !radioButtonArmyTwo.isSelected()) {
                throw new IllegalArgumentException("you have not selected any army to add unit(s)");
            }

            Army army = readFile();

            addUnits(army);

        }catch (Exception e){
            if(!Objects.equals(e.getMessage(), "file is null")  && !Objects.equals(e.getMessage(), "the file was never initialized")){
                Popups.showError("Loading error", "Could not read file",e.getMessage());
            }
        }

    }

    /**
     * deletes the units of the ModelBattle army to the corresponding radio button,
     * and adds the units and changes the name to the army.
     * ModelBattle army copies the army.
     * @param army The army one of the ModdleBattle armies shall copy.
     */
    private void addUnits(Army army){
        if(radioButtonArmyOne.isSelected()){

            modelBattle.getArmyOne().getUnits().clear();
            modelBattle.getArmyOne().addAll(army.getUnits());
            modelBattle.getArmyOne().setName(army.getName());
            fillArmyFields(modelBattle.getArmyOne());

        }else if(radioButtonArmyTwo.isSelected()){

            modelBattle.getArmyTwo().getUnits().clear();
            modelBattle.getArmyTwo().addAll(army.getUnits());
            modelBattle.getArmyTwo().setName(army.getName());
            fillArmyFields(modelBattle.getArmyTwo());

        }
    }

}
