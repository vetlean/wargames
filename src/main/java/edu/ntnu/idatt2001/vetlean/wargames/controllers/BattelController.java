package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.View;
import edu.ntnu.idatt2001.vetlean.wargames.units.CommanderUnit;
import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.models.Battle;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 * The battle controller controls the battle simulation
 * and updates the interface under simulation.
 */

public class BattelController extends BaseController {

    @FXML
    private TextArea armyOneTextArea,armyTwoTextArea;
    @FXML
    private Label nameOfArmyOne, nameOfArmyTwo;
    @FXML
    private TextField remainingCommander, remainingRnaged, remainingInfantry, remainingCavalry;
    @FXML
    private ProgressBar healthBarArmyOne, healthBarArmyTwo;
    @FXML
    private Label pointsArmyOne, pointsArmyTwo;
    @FXML
    private Button  addUnitsArmyOne, addUnitsArmyTwo;
    @FXML
    private Label theWinner,labelTerrain;

    private AnimationTimer loop;
    private long delayTime = 0;

    Battle battle = null;

    /**
     *  sets the battle attributes equal to a deep copy of the modelBattle battle.
     *  fills the text fields with armies form the deep copied battle.
     *  and initializes an animation timer to the attribute loop with an anonymous class.
     *  The game loop updates the interface with 5 fps.
     *  it runs until there is a winner or the game is paused.
     */
    public void initialize(){

        battle = Battle.getCopyOfBattel(modelBattle.getBattle());
        nameOfArmyOne.setText(battle.getArmyOne().getName());
        nameOfArmyTwo.setText(battle.getArmyTwo().getName());
        fillArmyFields(battle.getArmyOne());
        fillArmyFields(battle.getArmyTwo());
        addUnitsArmyOne.setText("add units to " + battle.getArmyOne().getName());
        addUnitsArmyTwo.setText("add units to " + battle.getArmyTwo().getName());
        labelTerrain.setText("The terrain is " + modelBattle.getBattle().getTerrain());

        loop = new AnimationTimer(){

            private final int DRAW_UPDATE_FPS = 5; // Amount of times per second that it should update the frontend.
            private final long FRAME_INTERVAL =  1_000_000_000L / DRAW_UPDATE_FPS;
            private long last = 0;
            private long start = 0;
            private int i = 0;

            @Override
            public void handle(long now) {
                if (start == 0){
                    start = now;
                }

                boolean isDone = !battle.getArmyOne().hasUnits() || !battle.getArmyTwo().hasUnits();

                if ((now - last > FRAME_INTERVAL) || isDone) {
                    fillArmyFields(battle.getArmyTwo());
                    fillArmyFields(battle.getArmyOne());
                    last = now;
                }

                if (isDone)  {
                    Army winner = battle.getArmyOne().hasUnits() ? battle.getArmyOne() : battle.getArmyTwo();
                    loop.stop();
                    fillWinner(winner);
                    theWinner.setText("The winner is " + winner.getName());
                } else {
                    battle.simulate();
                }
                i++;
            }
        };
    }

    /**
     * switches the scene to MAKE_ARMY_VIEW
     * @param event the event of press the button
     */
    public void switchToSceneMakeArmy(ActionEvent event){
        view.setCurrentScene(View.MAKE_ARMY_VIEW);
    }

    /**
     * switches the scene to READ_ARMY_FILE_VIEW
     * @param event the event of press the button
     */
    public void switchToReadArmyFromFileView(ActionEvent event) {
        view.setCurrentScene(View.READ_ARMY_FILE_VIEW);
    }

    /**
     * switches the scene to OPENING_VIEW
     * @param event the event of press the button
     */
    public void switchToOpeningView(ActionEvent event) {
        view.setCurrentScene(View.OPENING_VIEW);
    }

    /**
     * takes the size of the List of units and divides it on the orginal size.
     * this is a rough estimation the health of the army.
     * @param army The army on which we want to look at the state of health
     * @return a double representing the percentage og health left.
     */
    private double getPercentageOfUnitsLeft(Army army){
        double percentageOfUnitsLeft;

        try {
            if(army == battle.getArmyOne()){
                percentageOfUnitsLeft = (double) army.getUnits().size() / modelBattle.getArmyOne().getUnits().size() ;
            }else {
                percentageOfUnitsLeft = (double) army.getUnits().size() / modelBattle.getArmyTwo().getUnits().size();
            }
        }catch (ArithmeticException e){
            percentageOfUnitsLeft = 0;
        }

        return percentageOfUnitsLeft;
    }

    /**
     * The method that fills the army fields with an string representation of an army.
     * @param army The army that shall be represented to the user.
     */
    private void fillArmyFields(Army army){
        String armyAsString = Util.representationOfArmy(army);

        if (army == battle.getArmyOne()){
            armyOneTextArea.setText(armyAsString);
            double percentageOfUnitsLeft = getPercentageOfUnitsLeft(army);
            healthBarArmyOne.setProgress(percentageOfUnitsLeft);
            pointsArmyOne.setText("points: " + battle.getArmyOnePoints());
        }else {
            double percentageOfUnitsLeft = getPercentageOfUnitsLeft(army);
            armyTwoTextArea.setText(armyAsString);
            healthBarArmyTwo.setProgress(percentageOfUnitsLeft);
            pointsArmyTwo.setText("points: " + battle.getArmyTwoPoints());
        }

    }

    /**
     * fills the riled og the winning army
     * @param army the army that won
     */
    private void fillWinner(Army army){

        remainingCommander.setText(String.valueOf(army.getCommanderUnits().size()));
        remainingRnaged.setText(String.valueOf(army.getRangedUnits().size()));
        remainingInfantry.setText(String.valueOf(army.getInfantryUnits().size()));
        remainingCavalry.setText(String.valueOf(army.getCavalryUnits().size()));

    }

    /**
     * starts the game loop
     */
    public void startBattle(){
        loop.start();
    }

    /**
     * stops the game loop.
     */
    public void stopBattle(){
        loop.stop();
        fillArmyFields(battle.getArmyOne());
        fillArmyFields(battle.getArmyTwo());
    }

    /**
     * deletes the whole battle and copies modelBattle battle again.
     */
    public void resetArmies() {
        battle = null;
        battle = Battle.getCopyOfBattel(modelBattle.getBattle());
        modelBattle.getBattle().changerToRandomTerrain();
        labelTerrain.setText("The terrain is " + modelBattle.getBattle().getTerrain());
        fillArmyFields(battle.getArmyOne());
        fillArmyFields(battle.getArmyTwo());
    }

    /**
     * the method checks if the army can afford the reinforcements.
     * if the army can it gets a commander unit.
     * @param points an integer representing the amount of points for the army
     * @param isDone a boolean representing, true if done, false if not
     * @param army the army getting reinforcements
     */
    private void sendReinforcements(int points, boolean isDone, Army army){

        if (points > 99 && !isDone){
            CommanderUnit commanderUnit = new CommanderUnit("Commander Unit",180);
            army.add(commanderUnit);

            if (army == battle.getArmyOne()){
                battle.getArmyOne().add(commanderUnit);
                battle.setArmyOnePoints(points-100);
            }else {
                battle.getArmyTwo().add(commanderUnit);
                battle.setArmyTwoPoints(points-100);
            }
            fillArmyFields(army);
        }
    }

    /**
     * Adds a predefined commander unit to army one if it has enough points(100)
     */
    public void addUnitsToArmyOne(){
        int points = battle.getArmyOnePoints();
        boolean isDone = !battle.getArmyOne().hasUnits() || !battle.getArmyTwo().hasUnits();

        sendReinforcements(points, isDone, battle.getArmyOne());
    }

    /**
     * Adds a predefined commander unit to army two if it has enough points(100)
     */
    public void addUnitsToArmyTwo(){
        int points = battle.getArmyTwoPoints();
        boolean isDone = !battle.getArmyOne().hasUnits() || !battle.getArmyTwo().hasUnits();

        sendReinforcements(points, isDone, battle.getArmyTwo());
    }

}
