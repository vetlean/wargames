package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.View;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;
import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.Popups;
import edu.ntnu.idatt2001.vetlean.wargames.factories.UnitFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MakeArmyController extends BaseController {

    @FXML
    private TextField amountOfCommanderUnits, amountOfCavalryUnit, amountOfInfantryUnit, amountOfRangedUnit;
    @FXML
    private RadioButton rArmyOneSave, rArmyTwoSave;
    @FXML
    private CheckBox checkBoxArmyOne,checkBoxArmyTwo;
    @FXML
    private TextArea armyOneTextArea,armyTwoTextArea;
    @FXML
    private TextField nameFieldCommander, nameFieldCavalry, nameFieldInfatry,nameFieldRanged;
    @FXML
    private TextField healthFieldCommander, healthFieldCavalry, healthFieldInfantry,healthFieldRanged;
    @FXML
    private TextField makeArmyOneText, makeArmyTwoText;
    @FXML
    private TextField saveArmyFiled;

    public void initialize(){
        fillArmyFields(modelBattle.getArmyOne());
        fillArmyFields(modelBattle.getArmyTwo());
    }

    /**
     * switches the scene to READ_ARMY_FILE_VIEW
     * @param event the event of press the button
     */
    public void switchReadArmyFile(ActionEvent event){
        view.setCurrentScene(View.READ_ARMY_FILE_VIEW);
    }

    /**
     * switches the scene to BATTEL_SIMULATION_VIEW
     * @param event the event of press the button
     */
    public void switchToBattelView(ActionEvent event) {
        view.setCurrentScene(View.BATTEL_SIMULATION_VIEW);
    }

    /**
     * switches the scene to OPENING_VIEW
     * @param event the event of press the button
     */
    public void switchToOpeningView(ActionEvent event) {
        view.setCurrentScene(View.OPENING_VIEW);
    }

    /**
     * saves the army
     * can only save one army at the time
     * uses the writeArmy method to save army
     */
    public void saveArmy(){

        try {
            if (saveArmyFiled.getText().isEmpty()){
                throw new IllegalArgumentException("To save an army you have to assignint a name to the file");
            }

            if(rArmyOneSave.isSelected()){
                File file = Util.saveArmyToFile(saveArmyFiled);
                armyFileHandler.writeArmy(modelBattle.getArmyOne(), file);

            }else if (rArmyTwoSave.isSelected()){
                File file = Util.saveArmyToFile(saveArmyFiled);
                armyFileHandler.writeArmy(modelBattle.getArmyTwo(), file);

            }

        }catch (Exception e){
            Popups.showError("Saving error", "The file has to have a name",e.getMessage());
        }
    }

    /**
     * fills the army field on the given window
     * gives information on what the armies contains
     * @param army the army you want to represent
     */
    private void fillArmyFields(Army army){

        String armyAsString = Util.representationOfArmyWithStats(army);

        if (army == modelBattle.getArmyOne()){
            armyOneTextArea.setText(armyAsString);
        }else {
            armyTwoTextArea.setText(armyAsString);
        }
    }

    /**
     * Finds the unit(s) the user wants to make and returns a string array with information needed to make the unit(s).
     * @param unitType a string representing the unit the user wants to make
     * @return a string array with information needed to make the unit(s)
     */
    private String[] correspondingNumberOfUnits(String unitType){

        if(unitType.equalsIgnoreCase("CommanderUnit")){
            return new String[]{amountOfCommanderUnits.getText(),nameFieldCommander.getText(), healthFieldCommander.getText()};
        } else if(unitType.equalsIgnoreCase("CavalryUnit")){
            return new String[]{amountOfCavalryUnit.getText(),nameFieldCavalry.getText(), healthFieldCavalry.getText()};
        }else if (unitType.equalsIgnoreCase("InfantryUnit")) {
            return new String[]{amountOfInfantryUnit.getText(),nameFieldInfatry.getText(), healthFieldInfantry.getText()};
        }else if(unitType.equalsIgnoreCase("RangedUnit")){
            return new String[]{amountOfRangedUnit.getText(),nameFieldRanged.getText(), healthFieldRanged.getText()};
        }else {
            throw new IllegalArgumentException("The unit you were searching does not exist, you searched for " + unitType);
        }

    }

    /**
     * makes an integer of the amount of units to make from tekst fields from the interface;
     * @param unitType a string representing the unit the user wants to make.
     * @return an integer representing the amount of units needed to be produced.
     * @throws NumberFormatException If the text field contains more than just numbers.
     */
    private int amountOfUnits(String unitType) throws NumberFormatException{

        if(unitType.equalsIgnoreCase("CommanderUnit")){
            return Integer.parseInt(amountOfCommanderUnits.getText());
        } else if(unitType.equalsIgnoreCase("CavalryUnit")){
            return Integer.parseInt(amountOfCavalryUnit.getText());
        }else if (unitType.equalsIgnoreCase("InfantryUnit")) {
            return Integer.parseInt(amountOfInfantryUnit.getText());
        }else if(unitType.equalsIgnoreCase("RangedUnit")){
            return Integer.parseInt(amountOfRangedUnit.getText());
        }else {
            throw new IllegalArgumentException("The unit you were searching does not exist, you searched for " + unitType);
        }

    }

    /**
     * makes an integer of the text fields from the interface.
     * @param stringHealth a string representing the health.
     * @return an integer representing the health value of the unit.
     * @throws IllegalArgumentException if the health is zero or lower.
     */
    private int getHealth(String stringHealth) throws IllegalArgumentException{

        String regex = "[0-9]+";

        if (stringHealth.isEmpty()){
            throw new IllegalArgumentException("To make a unit you have to assign it a value the attribute health"+ "\n"
                    + " you entered " + stringHealth );
        }

        if (!stringHealth.matches(regex)){
            throw new IllegalArgumentException("the health value of a unit must a number, you entered: \n" + stringHealth);
        }

        int health = Integer.parseInt(stringHealth);

        if(health <= 0){
            throw new IllegalArgumentException("The unit can not have a health value of zero or lower");
        }

        return health;
    }

    /**
     * makes the units the user specified
     * @return a list of units
     * @throws IllegalArgumentException if the user inputs wrong
     */
    public List<Unit> makeUnits(String unitType) throws IllegalArgumentException{

        int numberOfUnits = 0;

        try {
            numberOfUnits = amountOfUnits(unitType);
            if (numberOfUnits  < 0){
               throw new IllegalArgumentException("The amount of unitst must be positive integer");
            }
        }catch (NumberFormatException e){
            if(!e.getMessage().contains("\"\"")){
                throw new IllegalArgumentException("the amount of units must be a number, \n" + e.getMessage());
            }
        }

        if (numberOfUnits > 0){

            String[] unitInfo = correspondingNumberOfUnits(unitType);
            String name = unitInfo[1];

            if(name.isEmpty()){
                throw new IllegalArgumentException("The unit must have a name, i can not be blank");
            }

            int health = getHealth(unitInfo[2]);
            return UnitFactory.makeUnitLists(unitType, name, health, numberOfUnits);
        }

        return new ArrayList<Unit>();
    }


    /**
     * The bigest army can only be 5000 units.
     * cheks if the army will surpass 5000.
     * @param unitsToAdd the List of units intended to be added.
     * @param army the army the units intend be added to.
     */
    private void testSize(List<Unit> unitsToAdd, Army army){

        int totalNumberOfUnits = unitsToAdd.size() + army.getUnits().size();

        if (totalNumberOfUnits > 5000) {
            throw new IllegalArgumentException("The maximum number of units to an army is 5000"+ "\n"
                    + "you try to make a army of " + totalNumberOfUnits);
        }

    }

    /**
     * finds the units to delete when user modefies the army.
     * @param unitType a string representing the type of units to be modified in the army.
     * @param army the army that will be modified.
     * @return a list off al the exiting units in the army that will be deleted.
     */
    public List<Unit> findeUnitsTodDelete(String unitType, Army army){

        if(unitType.equalsIgnoreCase("CommanderUnit")){
            return army.getCommanderUnits();
        } else if(unitType.equalsIgnoreCase("CavalryUnit")){
            return army.getCavalryUnits();
        }else if (unitType.equalsIgnoreCase("InfantryUnit")) {
            return army.getInfantryUnits();
        }else if(unitType.equalsIgnoreCase("RangedUnit")){
            return army.getRangedUnits();
        }else {
            throw new IllegalArgumentException("Did nor find the units to delete");
        }

    }

    /**
     * adds the units to the army.
     * @param army The army the unit will be added to
     */
    private void addUnits(Army army){

        String[] differentUnits = new String[] {"CommanderUnit", "CavalryUnit", "InfantryUnit","RangedUnit" };

        List<Unit> unitsToAdd;
        List<Unit> unitsToDelite;

        for (String typeUnit : differentUnits){
            unitsToAdd = makeUnits(typeUnit);
            unitsToDelite = findeUnitsTodDelete(typeUnit, army);
            testSize(unitsToAdd,army);
            army.getUnits().removeAll(unitsToDelite);
            army.addAll(unitsToAdd);
        }

        fillArmyFields(army);
    }

    /**
     *
     * changes the name of the armies from the standard name.
     */
    public void giveNameToArmy(){

        if (!makeArmyOneText.getText().isEmpty()){
            modelBattle.getArmyOne().setName(makeArmyOneText.getText());
            fillArmyFields(modelBattle.getArmyOne());
        }

        if (!makeArmyTwoText.getText().isEmpty()){
            modelBattle.getBattle().getArmyTwo().setName(makeArmyTwoText.getText());
            fillArmyFields(modelBattle.getArmyTwo());
        }

    }

    /**
     * adds the units to the given army
     */
    public void addUnitsToArmy(){

        try {

            if (!checkBoxArmyOne.isSelected() && !checkBoxArmyTwo.isSelected()) {
                throw new IllegalArgumentException("you have not selected any army to add unit(s)");
            }

            if (checkBoxArmyOne.isSelected()) {
                addUnits(modelBattle.getArmyOne());
            }

            if (checkBoxArmyTwo.isSelected()) {
                addUnits(modelBattle.getArmyTwo());
            }

            giveNameToArmy();

        }catch (IllegalArgumentException e){
            Popups.showError("error with adding unit(s)",  "An error occurred while adding unit(s) ", e.getMessage() );
        }
    }
}
