package edu.ntnu.idatt2001.vetlean.wargames;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * this class is inspired form team 5 cohort 2 project from IDATT1002,
 * this class is a sinelgton class for managing the current view.
 */

public class View {


    private static View instance;

    public final static String OPENING_VIEW = "view/Opening-view.fxml";
    public final static String MAKE_ARMY_VIEW = "view/make-army-view.fxml";
    public final static String BATTEL_SIMULATION_VIEW = "view/battel-simulation-view.fxml";
    public final static String READ_ARMY_FILE_VIEW = "view/read-army-file-view.fxml";

    private Stage stage;
    private Scene scene;

    /**
     * an empty and private constructor to make the class singelton
     */
    private View(){
    }

    /**
     * Method for getting the instance of the View class.
     * @return the instance
     */
    public static synchronized View getInstance(){
        if(instance == null){
            instance = new View();
        }
        return instance;
    }

    /**
     * Sets the current scene, it is preferred to use the static constants in this class to set a scene,
     * as they will be correct.
     * @param path to FXML file
     * @throws IllegalArgumentException if the path is wrong or the file is wrong.
     */
    public void setCurrentScene(String path) throws IllegalArgumentException{
        FXMLLoader fxmlLoader = new FXMLLoader(View.class.getResource(path));
        try {
            this.scene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Something went wrong with loading the FXML file:\n" + e.getMessage());
        }
        updateStage();
    }

    /**
     * changing the stage
     * @param stage the new stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * gets the stage
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * updates the stage
     */
    private void updateStage() {
        this.stage.setScene(this.scene);
    }

    /**
     * Method for setting the current scene, the scene is the content of the window.
     * @param currentScene
     */
    public void setCurrentScene(Scene currentScene) {
        this.scene = currentScene;
        updateStage();
    }
}
