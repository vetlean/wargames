package edu.ntnu.idatt2001.vetlean.wargames.models;

import java.util.Objects;

public class ModelBattle {

    private static ModelBattle instance;
    private Battle battle = null;
    private Army armyOne = null;
    private Army armyTwo = null;

    private ModelBattle(){
    }

    public static ModelBattle getInstance(){
        if(instance == null){
            instance = new ModelBattle();
        }
        return instance;
    }

    public void setBattle(){
        if (this.battle == null){
            this.battle = new Battle(armyOne,armyTwo);
        }
    }

    public Battle getBattle() {
        return battle;
    }

    public void setArmyOne(Army armyOne) {

        if (Objects.isNull(armyOne)){
            throw new IllegalArgumentException("The army that is added is null");
        }

        if (this.armyOne == null){
            this.armyOne = armyOne;
        }
    }

    public void setArmyTwo(Army armyTwo) {

        if (Objects.isNull(armyTwo)){
            throw new IllegalArgumentException("The army that is added is null");
        }

        if (this.armyTwo == null){
            this.armyTwo = armyTwo;
        }
    }

    public Army getArmyOne() {
        return armyOne;
    }

    public Army getArmyTwo(){
        return armyTwo;
    }
}
