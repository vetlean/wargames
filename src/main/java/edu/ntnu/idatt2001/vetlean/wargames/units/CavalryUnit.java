package edu.ntnu.idatt2001.vetlean.wargames.units;

public class CavalryUnit extends Unit {
    /**
     * attackCounter keeps track of how many times the unit has been attacked
     */
    private int attackCounter = 0;

    /**
     * the constructor of the unit class, it has preset values on accuracy(0.80)
     * and counterattack(0.20)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor,0.8,0.20);
    }

    /**
     * the simple constructor, it has preset values on attack(20), armor(12), accuracy(0.80)
     * and counterattack(0.20)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     */
    public CavalryUnit(String name, int health){
        super(name, health, 20, 12,0.8, 0.20);
    }

    /**
     * A constructor, it makes it possible to give a commander unit a higher accuracy.
     * this constructor is good for testing, because it can make a unit with zero and
     * one hundred percent accuracy and counterattack.
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     * @param accuracy a double representing the percentage of hits
     * @param counterattack a double representing the percentage of time the unit counter an attack
     */
    public CavalryUnit(String name, int health, int attack, int armor, double accuracy, double counterattack) {
        super(name, health, attack, armor, accuracy, counterattack);
    }

    /**
     * the attack bonus decreases after the amount of times the unit has been attacked.
     * @return integer, representing the attack bonus
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        
        int startAttackBonus = 6;
        
        if(terrain == Terrain.PLAINS){
            startAttackBonus = 10;
        }
        
        switch (attackCounter){
            case 0:
                return startAttackBonus; // 6
            case 1:
                return startAttackBonus - 2; //4
            case 2:
                return startAttackBonus - 3; // 3
            case 3:
                return startAttackBonus - 4; // 2
            default:
                return startAttackBonus - 6; // 0
        }
    }

    /**
     * @return integer, representing the armour bonus.
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        if (terrain == Terrain.FOREST){
            return 0;
        }

        return 1;
    }

    /**
     * when the unit attacks it increases the attack counter by one.
     * @param opponent the unit that's being attacked.
     * @param terrain representing the terrain the units are fighting on.
     */
    @Override
    public void attack(Unit opponent, Terrain terrain) {
        super.attack(opponent, terrain);
        attackCounter +=1;
    }

    /**
     * this method does not take in the paramter of accuracy.
     * This method is for testing if one attack hits it deals the damage that is expected.
     * @param opponent the unit that's being attacked.
     * @param terrain representing the terrain the units are fighting on.
     */
    @Override
    public void testAttack(Unit opponent, Terrain terrain) {
        super.testAttack(opponent, terrain);
        attackCounter +=1;
    }
}
