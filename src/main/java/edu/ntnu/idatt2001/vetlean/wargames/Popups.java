package edu.ntnu.idatt2001.vetlean.wargames;

import javafx.scene.control.Alert;

public class Popups {

    /**
     * makes an error alert window for the user to see
     * @param title the title of the popup window as a string
     * @param header the header of the popup window as a string
     * @param content the content/main message to the user
     */
    public static void showError(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.show();
    }
}
