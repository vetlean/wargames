package edu.ntnu.idatt2001.vetlean.wargames.factories;

import edu.ntnu.idatt2001.vetlean.wargames.units.*;

import java.util.ArrayList;
import java.util.List;

public abstract class UnitFactory {

    /**
     * makes one unit of the desired unit
     * @param unitType the name of the class you want to make as String
     * @param name the name of the army as String
     * @param health the amount of health as int
     * @return a unit based on the specification
     */
    public static Unit makeUnit(String unitType, String name, int health){

        Unit unit = findClass(unitType);
        unit.setName(name);
        unit.setHealth(health);

        return unit;
    }


    /**
     * makes a list of units of the desired unit type,
     * @param unitType the name of the class you want to make as String
     * @param name the name of the army as String
     * @param health the amount of health as int
     * @param amount the number of units in the list you ordered
     * @return a list of units based on the specification
     */
    public static List<Unit> makeUnitLists(String unitType, String name, int health, int amount) throws IllegalArgumentException{
        List<Unit> units = new ArrayList<>();

        for (int i = 0; i < amount; i++){

            Unit unit = findClass(unitType);
            unit.setName(name);
            unit.setHealth(health);

            units.add(unit);
        }
        return units;
    }

    /**
     *
     * makes a list of units of the desired unit type
     * @param unitType the name of the class you want to make as String
     * @param name the name of the army as String
     * @param health the amount of health as int
     * @param attack the attck of the unit as int
     * @param armor the armor of the unit as int
     * @param amount the number of units in the list you ordered
     * @return a list of units based on the specification
     */
    public static List<Unit> makeUnitListSpecific(String unitType, String name, int health, int attack, int armor, int amount){
        List<Unit> units = new ArrayList<>();

        for (int i = 0; i < amount; i++){

            Unit unit = findClass(unitType);
            unit.setName(name);
            unit.setHealth(health);
            unit.setAttack(attack);
            unit.setArmor(armor);

            units.add(unit);
        }

        return units;
    }


    /**
     * makes a dummy unit of the desired class given the naem of the class.
     * the dummy unit will be modified to the specifications later
     * @param clazz a String representation of the class
     * @return a dummy unit to be modified
     */
    private static Unit findClass(String clazz){
        if(clazz.equalsIgnoreCase("InfantryUnit")){
            return new InfantryUnit("name",10);
        }else if (clazz.equalsIgnoreCase("CavalryUnit")){
            return new CavalryUnit("name",10);
        }
        else if (clazz.equalsIgnoreCase("RangedUnit")){
            return new RangedUnit("name",10);

        }else if (clazz.equalsIgnoreCase("CommanderUnit")){
            return new CommanderUnit("name", 10);
        }else {
            throw new IllegalArgumentException("did not find class, the name of the specific unit was wrong.");
        }
    }
}
