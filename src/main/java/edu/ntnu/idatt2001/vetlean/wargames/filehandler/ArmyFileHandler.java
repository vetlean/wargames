package edu.ntnu.idatt2001.vetlean.wargames.filehandler;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ArmyFileHandler {

    private static final String NEWLINE = "\n";
    private static final String COMMA = ",";

    /**
     * The constructor that initializes an object from class ArmyFileHandler
     */
    public ArmyFileHandler(){};

    /**
     * writes an object to a file. It takes name of the army on top of the text file
     * and loops through the name of the unit and health of the unit.
     * @param army The object want to write to a file.
     * @param file The CSV file you wants to write object to.
     * @throws IOException
     * <p>
     * Sample file data:
     * <pre>{@code}
     * Army name
     * unit type,unit name,unit health
     * unit type,unit name,unit health
     * unit type,unit name,unit health
     * {@code}</pre>
     * </p>
     */
    public void writeArmy(Army army, File file) throws IOException {

        if(!file.getName().endsWith(".csv")){
            throw new IOException("Unsupported file format. Only .csv-files are supported");
        }
        Objects.requireNonNull(army, "Army can not be null");

        try (FileWriter fileWriter = new FileWriter(file)){
            fileWriter.write(army.getName()+NEWLINE);
            for (Unit unit : army.getUnits()){
                String clas = unit.getClass().toString();
                String line = clas.substring(clas.lastIndexOf('.')+1)+ COMMA+ unit.getName()+ COMMA+ unit.getHealth();
                fileWriter.write(line+NEWLINE);
            }
        }catch (IOException e){
            throw new IOException("unable to write army file. " + e.getMessage());
        }
    }

    /**
     * takes one csv file in the correct format and makes an object.
     * Loops through the csv file one line at the time and makes one unit.
     * in the end it takes the name and units and makes an army.
     * @param file the csv file you want to de deserializer.
     * @return an army that was made form the file.
     * @throws IOException throws an IOException if there is somthing wrong with format
     */
    public Army readArmy(File file) throws IOException{
        Army army;
        Unit unit = null;

        if (Objects.isNull(file)){
            throw new IOException("file is null");
        }

        if (!file.getAbsolutePath().endsWith(".csv")){
            throw new IOException("Unsupported file format. Only .csv-files are supported");
        }
        
        List<Unit> units = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {

            String nameOfArmy = null;

            if(scanner.hasNext()){
                nameOfArmy = scanner.nextLine();
            }else {
                throw new NullPointerException("The file is empty, it contains no data.");
            }

            if(nameOfArmy.isBlank() || nameOfArmy.contains(",")){
                throw new IOException("Wrong format, the army must have a name");
            }

            while (scanner.hasNext()) {

                String line = scanner.nextLine();
                String[] tokens = line.split(COMMA);

                if (tokens.length != 3) {
                    throw new IOException("Wrong format, the csv file is not structured properly");
                }

                int health;

                try {
                    health = Integer.parseInt(tokens[2]);
                } catch (NumberFormatException e) {
                    throw new NumberFormatException("Wrong format, health should be placed last, as number 3");
                }

                String name = tokens[1];
                String nameOfClass = tokens[0];

                try {
                    unit = findClass(nameOfClass);
                    unit.setHealth(health);
                    unit.setName(name);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }
                
                units.add(unit);
            }
            army = new Army(nameOfArmy, units);
            return army;
        }
    }

    /**
     * Finds the class the unit belongs to.
     * @param clazz the name of the class
     * @return a dummy of a unit that will be changed.
     */
    public Unit findClass(String clazz){
        if(clazz.equalsIgnoreCase("InfantryUnit")){
            return new InfantryUnit("name",10);
        }else if (clazz.equalsIgnoreCase("CavalryUnit")){
            return new CavalryUnit("name",10);
        }
        else if (clazz.equalsIgnoreCase("RangedUnit")){
            return new RangedUnit("name",10);
            
        }else if (clazz.equalsIgnoreCase("CommanderUnit")){
            return new CommanderUnit("name", 10);
        }else {
            throw new IllegalArgumentException("did not find class");
        }
    }
}