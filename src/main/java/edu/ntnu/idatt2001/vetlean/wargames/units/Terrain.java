package edu.ntnu.idatt2001.vetlean.wargames.units;

/**
 * An enum klass to define all the different types of terrain
 */
public enum Terrain {

    HILL,
    PLAINS,
    FOREST;

}
