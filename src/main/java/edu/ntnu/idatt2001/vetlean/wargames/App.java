package edu.ntnu.idatt2001.vetlean.wargames;

//579745ba-95a6-4952-ae09-36438e9d4105

import javafx.application.Application;
import javafx.stage.Stage;


import java.io.IOException;

public class App extends Application {

    private final double MAX_RES_WIDTH = 800.0;
    private final double MAX_RES_HEIGHT = 1000.0;
    private final double MIN_RES_WIDTH = 800.0;
    private final double MIN_RES_HEIGHT = 1000.0;

    public static void main(String[] args) {
        launch();
    }

    /**
     * the start method that starts the window
     * @param primaryStage the main stage
     * @throws IOException if it can not read the FXML file
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        View view = View.getInstance();
        view.setStage(primaryStage);
        view.setCurrentScene(View.OPENING_VIEW);
        setAppearance(primaryStage);
        primaryStage.show();
    }

    /**
     * sets the window to a foxed size
     * @param stage the main stage
     */
    private void setAppearance(Stage stage){
        stage.setTitle("Wargames");
        stage.setResizable(false);
        stage.setMaxWidth(MAX_RES_WIDTH);
        stage.setMaxHeight(MAX_RES_HEIGHT);
        stage.setMinHeight(MIN_RES_WIDTH);
        stage.setMinWidth(MIN_RES_HEIGHT);
    }
}
