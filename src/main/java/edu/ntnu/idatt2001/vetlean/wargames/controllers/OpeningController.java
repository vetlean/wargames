package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.View;
import javafx.event.ActionEvent;

public class OpeningController extends BaseController {

    /**
     * initializes the objects in models
     * this assures the creation of the modelBattle is corect.
     */
    public void initialize(){
        modelBattle.setArmyOne(new Army("Army one"));
        modelBattle.setArmyTwo(new Army("Army two"));
        modelBattle.setBattle();
    }

    /**
     * switches the scene to MAKE_ARMY_VIEW
     * @param event the event of press the button
     */
    public void switchToSceneMakeArmy(ActionEvent event){
        view.setCurrentScene(View.MAKE_ARMY_VIEW);
    }

    /**
     * switches the scene to READ_ARMY_FILE_VIEW
     * @param event the event of press the button
     */
    public void switchToSceneReadArmyFormFile(ActionEvent event){
        view.setCurrentScene(View.READ_ARMY_FILE_VIEW);
    }
}
