package edu.ntnu.idatt2001.vetlean.wargames.units;

public class InfantryUnit extends Unit {

    /**
     * the constructor of the Infantry unit class it has a preset accuracy(0.85)
     * and counterattack(0.15)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     */

    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor,0.85,  0.15);
    }
    /**
     * the simple constructor, it has preset values on attack(15), armor(10), accuracy(0.85)
     * and counterattack(0.15)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     */
    public InfantryUnit(String name, int health){
        super(name, health, 15,10,0.85,  0.15);
    }

    /**
     * @return integer, representing the attack bonus.
     * @param terrain representing the terrain the unit is fighting on.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {

        if(terrain == Terrain.FOREST){
            return 4;
        }

        return 2;
    }

    /**
     * @return integer, representing the armour bonus.
     * @param terrain representing the terrain the unit is fighting on.
     */
    @Override
    public int getResistBonus(Terrain terrain) {

        if(terrain == Terrain.FOREST){
            return 3;
        }
        return 1;
    }
}
