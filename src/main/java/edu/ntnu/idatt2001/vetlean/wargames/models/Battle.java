package edu.ntnu.idatt2001.vetlean.wargames.models;

/**
 * this is the battle class.
 * this clas has the responsibility of simulating the war.
 */

import edu.ntnu.idatt2001.vetlean.wargames.units.Terrain;
import edu.ntnu.idatt2001.vetlean.wargames.units.CommanderUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;

import java.io.*;
import java.util.Objects;
import java.util.Random;

public class Battle implements Serializable {
    private final Army armyOne;
    private final Army armyTwo;
    private int armyOnePoints;
    private int armyTwoPoints;
    private final Random random;
    private Terrain terrain;

    /**
     * the constructor of the battle klasse
     * @param armyOne the first army that goes to war
     * @param armyTwo the second army that goes to war
     */
    public Battle(Army armyOne, Army armyTwo) {
        if(Objects.isNull(armyOne)){
            throw new NullPointerException("armyOne is null");
        }
        this.armyOne = armyOne;
        if (Objects.isNull(armyTwo)){
            throw new NullPointerException("armyTwo is null");
        }
        this.armyTwo = armyTwo;
        random = new Random();
        this.terrain = Terrain.values()[random.nextInt(Terrain.values().length)];
        this.armyOnePoints = 0;
        this.armyTwoPoints = 0;
    }

    /**
     * the constructor of the battle klasse
     * In this constructor there is a given terrain, not random.
     * @param armyOne the first army that goes to war
     * @param armyTwo the second army that goes to war
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        if(Objects.isNull(armyOne)){
            throw new NullPointerException("armyOne is null");
        }
        this.armyOne = armyOne;
        if (Objects.isNull(armyTwo)){
            throw new NullPointerException("armyTwo is null");
        }
        this.armyTwo = armyTwo;
        random = new Random();
        this.terrain = terrain;
        this.armyOnePoints = 0;
        this.armyTwoPoints = 0;
    }

    /**
     * make a copy of a battle object.
     * it serializes the object, saves it to the file.
     * makes an object from the file and then deletes it.
     * this makes sure the whole battle object is a deep copy
     * @param battleToCopy the battle you want to have deep copy of
     * @return a battle that is deep copied of the given battle
     */
    public static Battle getCopyOfBattel(Battle battleToCopy) {

        if(Objects.isNull(battleToCopy)){
            throw new IllegalArgumentException("The battle object you try to copy is null");
        }

        Battle battle = null;

        String path = "src/main/resources/edu/ntnu/idatt2001/vetlean/wargames/copyObject.ser";
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {

            objectOutputStream.writeObject(battleToCopy);

        } catch (IOException e) {
            throw new IllegalArgumentException("could not find the path where the object should have been saved");
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path));){

            battle = (Battle) objectInputStream.readObject();

        }catch(Exception e) {
            throw new IllegalArgumentException("could not find the path where the object should have been saved");
        }

        try {
            File file = new File(path);
            boolean isdelited  = file.delete();
            if (!isdelited){
                throw new IOException("The file was not deleted");
            }

        }catch (IOException e){
            throw new IllegalArgumentException("could not find the path where the object was saved");
        }
        return battle;
    }

    /**
     * a method to get the terrain of the battle
     * @return an enum representing the terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }

    public void changerToRandomTerrain(){
        this.terrain = Terrain.values()[random.nextInt(Terrain.values().length)];
    }

    /**
     * a method to get army one from battle
     * @return army one
     */
    public Army getArmyOne() {
        return armyOne;
    }

    /**
     * a method to get army two from battle
     * @return army two
     */
    public Army getArmyTwo(){
        return armyTwo;
    }

    /**
     * @return the amount of points for army one.
     */
    public int getArmyOnePoints() {
        return armyOnePoints;
    }

    /**
     * Sets the amount of points for army one
     * @param armyOnePoints an integer representing the new amount of point
     */
    public void setArmyOnePoints(int armyOnePoints) {
        this.armyOnePoints = armyOnePoints;
    }

    /**
     * @return the amount of point for army one
     */
    public int getArmyTwoPoints() {
        return armyTwoPoints;
    }

    /**
     * Sets the amount of points for army two
     * @param armyTwoPoints an integer representing the new amount of point
     */
    public void setArmyTwoPoints(int armyTwoPoints) {
        this.armyTwoPoints = armyTwoPoints;
    }

    /**
     * Simulates the one attack between two armies.
     * find one random unit from each army and repeat attack until one unit is dead.
     * gives one point if the army killed a "regular" unit and two for a commander unit.
     * The controller BattleController has the repetition of the attacks in a game loop.
     * this makes it possible to update the window while simulating.
     */
    public void simulate(){

        Unit strikeFirst;
        Unit respond;
        Unit dead;

        boolean armyOneFirst = random.nextInt(100) < 50;

        if(armyOneFirst){
            strikeFirst = armyOne.getRandom();
            respond = armyTwo.getRandom();
            dead = attackRepetition(strikeFirst,respond);
        }else {
            strikeFirst = armyTwo.getRandom();
            respond = armyOne.getRandom();
            dead = attackRepetition(strikeFirst,respond);
        }

        if (armyOne.getUnits().contains(dead)){
            if (dead.getClass() == CommanderUnit.class){
                armyTwoPoints += 2;
            }else {
                armyTwoPoints += 1;
            }
        }else {
            if (dead.getClass() == CommanderUnit.class){
                armyOnePoints += 2;
            }else {
                armyOnePoints += 1;
            }
        }

        if(armyOne.getUnits().contains(dead)){
            armyOne.remove(dead);
        }else {
            armyTwo.remove(dead);
        }

    }

    /**
     * repeats the attack the between to units to one is dead.
     * @param strikeFirst the unit that was got the first strike
     * @param respond the unit that got attacked
     * @return a unit. the winner between the two units.
     */
    public Unit attackRepetition(Unit strikeFirst, Unit respond){

        while (strikeFirst.getHealth() != 0){

            strikeFirst.attack(respond, this.terrain);
            if(respond.getHealth() == 0){
                return respond;
            }
            respond.attack(strikeFirst, this.terrain);
        }
        return strikeFirst;
    }

    /**
     *
     * @return string, representing the battle class.
     */
    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }
}
