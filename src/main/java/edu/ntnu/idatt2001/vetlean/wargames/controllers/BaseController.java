package edu.ntnu.idatt2001.vetlean.wargames.controllers;

import edu.ntnu.idatt2001.vetlean.wargames.filehandler.ArmyFileHandler;
import edu.ntnu.idatt2001.vetlean.wargames.models.ModelBattle;
import edu.ntnu.idatt2001.vetlean.wargames.View;

/**
 * This class is an abstract class that all controllers inherit.
 * This gives every controller full access to the data
 */

abstract public class BaseController {

    static ModelBattle modelBattle = ModelBattle.getInstance();
    static View view = View.getInstance();
    static ArmyFileHandler armyFileHandler = new ArmyFileHandler();

}
