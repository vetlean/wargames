package edu.ntnu.idatt2001.vetlean.wargames.units;

public class CommanderUnit extends CavalryUnit {

    /**
     * the constructor, it has preset values on attack(25), armor(15), accuracy(0.95)
     * and counterattack(0.25)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     */

    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor,0.95, 0.25);

    }

    /**
     * the simple constructor, it has preset values on attack(25), armor(15), accuracy(0.95)
     * and counterattack(0.25)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health,25,15,0.95,0.25);
    }
}
