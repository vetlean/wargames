package edu.ntnu.idatt2001.vetlean.wargames.units;

public class RangedUnit extends Unit {
    /**
     * attackCounter keeps track of how many times the unit has been attacked
     */
    private int attackedCounter = 0;

    /**
     * the constructor of the unit class.accuracy(0.90)
     * and counterattack(0.10)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     * @param attack an integer representing the attack damage one unit can give
     * @param armor an integer representing how mutch the armor can take of damage
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor, 0.90,  0.10);
    }

    /**
     * the simple constructor, it has preset values on attack(15), armor(8), accuracy(0.95)
     * and counterattack(0.25)
     * @param name a string representing the name of the unit
     * @param health an integer representing the health of the unit
     */
    public RangedUnit(String name, int health){
        super(name, health, 15, 8,0.90,0.10);
    }

    /**
     * @return integer, representing the attack bonus.
     * @param terrain representing the terrain the unit is fighting on.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {

        switch (terrain){
            case FOREST:
                return 1;
            case HILL:
                return 5;
            default:
                return 3;
        }
    }

    /**
     * the armour bonus decreases after the first attack
     * Terrain does not affect resistans bonus.
     * @return integer, representing the armour bonus.
     * @param terrain representing the terrain the unit is fighting on.
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        switch (attackedCounter){
            case 0:
                return 6;
            case 1:
                return 4;
            case 2:
                return 3;
            case 3:
                return 2;
            default:
                return 1;
        }
    }

    /**
     * when the unit changes its health it has been attack.
     * the attack counter increases by one.
     * @param health an ineger represnting the healt of the unit
     */
    @Override
    public void setHealth(int health) {
        super.setHealth(health);
        attackedCounter +=1;
    }


}
