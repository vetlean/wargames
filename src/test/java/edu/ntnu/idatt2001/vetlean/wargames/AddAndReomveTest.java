package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.CavalryUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class AddAndReomveTest {

    public Army testData(){
        ArrayList<Unit> units = new ArrayList<>();
        CavalryUnit ork1 = new CavalryUnit("Ork1",10);
        CavalryUnit ork2 = new CavalryUnit("Ork2",10);
        CavalryUnit ork3 = new CavalryUnit("Ork3",10);
        CavalryUnit ork4 = new CavalryUnit("Ork4",10);
        CavalryUnit ork5 = new CavalryUnit("Ork5",10);
        units.add(ork1);
        units.add(ork2);
        units.add(ork3);
        units.add(ork4);
        units.add(ork5);

        CavalryUnit human1 = new CavalryUnit("Human1",10);
        CavalryUnit human2 = new CavalryUnit("Human2",10);
        CavalryUnit human3 = new CavalryUnit("Human3",10);
        CavalryUnit human4 = new CavalryUnit("Human4",10);
        CavalryUnit human5 = new CavalryUnit("Human5",10);
        units.add(human1);
        units.add(human2);
        units.add(human3);
        units.add(human4);
        units.add(human5);

        Army armyOne = new Army("Første Army", units);

        return armyOne;

    }

    @Test
    @DisplayName("Remove 5 units from an Army with 10 units")
    public void removeFiveUnitTest(){
        Army army = testData();

        for(int i = 0; i<5; i++){
            Unit unit = army.getRandom();
            army.remove(unit);
        }

        assertEquals(5, army.getUnits().size());
    }

    @Test
    @DisplayName("Remove 10 units from an Arny with 10 units")
    public void removeAllUnitsFromArmyTest(){
        Army army = testData();
        int armySize = army.getUnits().size();
        for(int i = 0; i<armySize; i++){
            Unit unit = army.getRandom();
            army.remove(unit);
        }

        assertEquals(0, army.getUnits().size());
    }

    @Test
    @DisplayName("Army try to remove null object. Expects NullPointerExeption")
    public void removeNullObjectTest(){

        try {
            Unit unit = null;
            Army army = testData();
            army.remove(unit);
            fail();
        }catch (NullPointerException e){
            assertEquals(e.getMessage(), "The object you try to remove is empty(null)");
        }

        assertThrows(NullPointerException.class, () -> {
            Unit unit = null;
            Army army = testData();
            army.remove(unit);
        });
    }

    @Test
    @DisplayName("Army try to add null object, expects nullPoinetExeption")
    public void addNullObjectTest(){

        try {
                Unit unit = null;
                Army army = testData();
                army.add(unit);
                fail();
        }catch (NullPointerException e){
            assertEquals(e.getMessage(), "The object you try to add is empty(null)");
        }

        assertThrows(NullPointerException.class, () -> {
            Unit unit = null;
            Army army = testData();
            army.add(unit);
        });
    }

    @Test
    @DisplayName("add one unit to an army at the time")
    public void addOneUnitAtTheTime(){
        Army army = testData();

        CavalryUnit ork1 = new CavalryUnit("Ork1",10);
        army.add(ork1);
        CavalryUnit ork2 = new CavalryUnit("Ork2",10);
        army.add(ork1);
        CavalryUnit ork3 = new CavalryUnit("Ork3",10);
        army.add(ork3);
        CavalryUnit ork4 = new CavalryUnit("Ork4",10);
        army.add(ork4);
        CavalryUnit ork5 = new CavalryUnit("Ork5",10);
        army.add(ork5);

        assertEquals(15, army.getUnits().size());
    }

    @Test
    @DisplayName("add an existing list of units to the army")
    public void addAnExistingListOfUnitsToTheArmy(){
        Army army = testData();

        ArrayList<Unit> units = new ArrayList<>();
        CavalryUnit ork1 = new CavalryUnit("Ork1",10);
        CavalryUnit ork2 = new CavalryUnit("Ork2",10);
        CavalryUnit ork3 = new CavalryUnit("Ork3",10);
        CavalryUnit ork4 = new CavalryUnit("Ork4",10);
        CavalryUnit ork5 = new CavalryUnit("Ork5",10);
        units.add(ork1);
        units.add(ork2);
        units.add(ork3);
        units.add(ork4);
        units.add(ork5);

        army.addAll(units);

        assertEquals(15, army.getUnits().size());

    }


}
