package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.factories.UnitFactory;
import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.models.Battle;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class BattleTest {

    public ArrayList<Army> testData(){

        ArrayList<Unit> unitsOne = new ArrayList<>();
        ArrayList<Unit> infantryUnits = (ArrayList<Unit>) UnitFactory.makeUnitLists("InfantryUnit", "Footman",100,500);;
        ArrayList<Unit> commanderUnit = (ArrayList<Unit>) UnitFactory.makeUnitLists("commanderUnit","Mountain King", 180,10);
        ArrayList<Unit> rangedUnits = (ArrayList<Unit>) UnitFactory.makeUnitLists("rangedunit", "Archer",100, 200);
        ArrayList<Unit> cavalryUnit  = (ArrayList<Unit>) UnitFactory.makeUnitLists("cavalryUnit", "Knight", 100,100);

        unitsOne.addAll(infantryUnits);
        unitsOne.addAll(commanderUnit);
        unitsOne.addAll(rangedUnits);
        unitsOne.addAll(cavalryUnit);

        Army armyOne = new Army("Første Army", unitsOne);

        ArrayList<Unit> unitsTwo = new ArrayList<>();
        ArrayList<Unit> infantryUnitsTwo = (ArrayList<Unit>) UnitFactory.makeUnitLists("InfantryUnit", "Grunt",100,500);;
        ArrayList<Unit> commanderUnitTwo = (ArrayList<Unit>) UnitFactory.makeUnitLists("commanderUnit","Gul'dan", 180,10);
        ArrayList<Unit> rangedUnitsTwo = (ArrayList<Unit>) UnitFactory.makeUnitLists("rangedunit", "Spearman",100, 200);
        ArrayList<Unit> cavalryUnitTwo  = (ArrayList<Unit>) UnitFactory.makeUnitLists("cavalryUnit", "Raider", 100,100);
        unitsTwo.addAll(infantryUnitsTwo);
        unitsTwo.addAll(commanderUnitTwo);
        unitsTwo.addAll(rangedUnitsTwo);
        unitsTwo.addAll(cavalryUnitTwo);

        Army armyTwo = new Army("Andre Army", unitsTwo);

        ArrayList<Army> armies =  new ArrayList<>();
        armies.add(armyOne);
        armies.add(armyTwo);

        return armies;

    }

    @Test
    @DisplayName("Test if two armies can go to war and get one winner")
    public void testIfSimulationstepKillsOneArmy(){

        ArrayList<Army> armies = testData();
        Army armyOne = armies.get(0);
        Army armyTwo  = armies.get(1);

        Battle battle = new Battle(armyOne,armyTwo);

        Army winner;

        while (battle.getArmyOne().hasUnits() && battle.getArmyTwo().hasUnits()){

            battle.simulate();

        }

        if (battle.getArmyOne().hasUnits()){
            winner = battle.getArmyOne();
        }else {
            winner = battle.getArmyTwo();
        }

        assertNotNull(winner);
    }

    @Test
    public void testDeepCopyMethod(){
        ArrayList<Army> armies = testData();
        Army armyOne = armies.get(0);
        Army armyTwo  = armies.get(1);

        Battle battle = new Battle(armyOne,armyTwo);

        Battle deepCopyBattle = Battle.getCopyOfBattel(battle);

        assertNotSame(deepCopyBattle, battle);
        assertNotSame(deepCopyBattle.getArmyOne(), battle.getArmyOne());
        assertNotSame(deepCopyBattle.getArmyTwo(), battle.getArmyTwo());
        assertNotSame(deepCopyBattle.getArmyOne().getUnits().get(0), battle.getArmyOne().getUnits().get(0));
    }

    @Test
    public void testDeepCopyMethodOnNullBattleObject(){

        Battle battle = null;

        try {
            Battle deepCopyBattle = Battle.getCopyOfBattel(battle);
        }catch (IllegalArgumentException e){
            assertEquals(e.getMessage(), "The battle object you try to copy is null");
        }

        assertThrows(IllegalArgumentException.class, () -> {
            Battle.getCopyOfBattel(battle);
        });

    }
}
