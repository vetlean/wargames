package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SortUnitTest {
    public Army makeTestDataArmyHuman(){

        ArrayList<Unit> units = new ArrayList<>();

        for(int i =0; i < 500; i++){
            InfantryUnit infantryUnit = new InfantryUnit("Footman "+i, 100);
            units.add(infantryUnit);
        }

        for(int i =0; i < 100; i++){
            CavalryUnit cavalryUnit= new CavalryUnit("Knight "+i, 100);
            units.add(cavalryUnit);
        }

        for (int i = 0; i < 200; i++){
            RangedUnit rangedUnit = new RangedUnit("Archer "+i,100);
            units.add(rangedUnit);
        }

        for(int i = 0; i<10; i++){
            CommanderUnit commanderUnit = new CommanderUnit("Mountain King", 180);
            units.add(commanderUnit);
        }

        return new Army("Humnas", units);
    }

    public ArrayList<Unit> makeTestDataInfantryUnits(){

        ArrayList<Unit> infantryUnits = new ArrayList<>();

        for(int i =0; i < 500; i++){
            InfantryUnit infantryUnit = new InfantryUnit("Footman "+i, 100);
            infantryUnits.add(infantryUnit);
        }
        return infantryUnits;
    }

    public ArrayList<Unit> makeTestDataCavalryUnit(){

        ArrayList<Unit> cavalryUnits = new ArrayList<>();

        for(int i =0; i < 100; i++){
            CavalryUnit cavalryUnit= new CavalryUnit("Knight "+i, 100);
            cavalryUnits.add(cavalryUnit);
        }

        return cavalryUnits;
    }

    public ArrayList<Unit> makeTestDataRangedUnit(){

        ArrayList<Unit> rangedUnits = new ArrayList<>();

        for (int i = 0; i < 200; i++){
            RangedUnit rangedUnit = new RangedUnit("Archer "+i,100);
            rangedUnits.add(rangedUnit);
        }

        return rangedUnits;
    }

    public ArrayList<Unit> makeTestDataCommanderUnit(){

        ArrayList<Unit> commanderUnits = new ArrayList<>();

        for(int i = 0; i<10; i++){
            CommanderUnit commanderUnit = new CommanderUnit("Mountain King", 180);
            commanderUnits.add(commanderUnit);
        }

        return commanderUnits;
    }

    @Test
    @DisplayName("test get infantry unit")
    public void testGetInfantryUnit(){
        Army army = makeTestDataArmyHuman();

        ArrayList<Unit> infantryUints = makeTestDataInfantryUnits();

        assertEquals(500, army.getInfantryUnits().size());
        assertEquals(infantryUints, army.getInfantryUnits());

    }

    @Test
    @DisplayName("test get cavalry unit")
    public void testGetCavalryUnit(){
        Army army = makeTestDataArmyHuman();

        ArrayList<Unit> cavalryUnits  = makeTestDataCavalryUnit();

        assertEquals(100, army.getCavalryUnits().size());
        assertEquals(cavalryUnits, army.getCavalryUnits());

    }

    @Test
    @DisplayName("test get ranged unit")
    public void testGetRangedUnit(){
        Army army = makeTestDataArmyHuman();
        ArrayList<Unit> rangedUnits  = makeTestDataRangedUnit();

        assertEquals(200, army.getRangedUnits().size());
        assertEquals(rangedUnits, army.getRangedUnits());
    }

    @Test
    @DisplayName("test get commander unit")
    public void testGetCommanderUnit(){
        Army army = makeTestDataArmyHuman();
        ArrayList<Unit> commanderUnits  = makeTestDataCommanderUnit();

        assertEquals(10, army.getCommanderUnits().size());
        assertEquals(commanderUnits,army.getCommanderUnits());
    }
}
