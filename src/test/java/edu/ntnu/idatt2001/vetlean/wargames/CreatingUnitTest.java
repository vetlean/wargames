package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreatingUnitTest {

    @Nested
    class CreationOfArmy{

        public ArrayList<Unit> TestDataMakeUnits(){

            ArrayList<Unit> units = new ArrayList<>();
            CavalryUnit ork1 = new CavalryUnit("Ork1",10);
            CavalryUnit ork2 = new CavalryUnit("Ork2",10);
            CavalryUnit ork3 = new CavalryUnit("Ork3",10);
            CavalryUnit ork4 = new CavalryUnit("Ork4",10);
            CavalryUnit ork5 = new CavalryUnit("Ork5",10);
            units.add(ork1);
            units.add(ork2);
            units.add(ork3);
            units.add(ork4);
            units.add(ork5);

            CavalryUnit human1 = new CavalryUnit("Human1",10);
            CavalryUnit human2 = new CavalryUnit("Human2",10);
            CavalryUnit human3 = new CavalryUnit("Human3",10);
            CavalryUnit human4 = new CavalryUnit("Human4",10);
            CavalryUnit human5 = new CavalryUnit("Human5",10);
            units.add(human1);
            units.add(human2);
            units.add(human3);
            units.add(human4);
            units.add(human5);

            return units;

        }

        @Test
        @DisplayName("Test if is crated as expected")
        public void creationOfSimpleArmy(){
            Army army = new Army("First Army");
            army.add(new CommanderUnit("commander",10));
            assertEquals(1,army.getUnits().size());
            assertNotNull(army);
        }

        @Test
        @DisplayName("Test if Army with list of units is crated as expected")
        public void creationOfArmy(){
            ArrayList<Unit> units = TestDataMakeUnits();

            Army army = new Army("First Army", units);
            assertEquals(10,army.getUnits().size());
            assertNotNull(army);
        }

        @Test
        @DisplayName("Test if to object are the same if they have the same name and units size")
        public void testEqualsMetoden(){
            Army armyOne = new Army("Army", TestDataMakeUnits());
            Army armyTwo = new Army("Army", TestDataMakeUnits());
            Army armyTree = new Army("Army");

            assertEquals(armyOne, armyTwo);
            assertNotEquals(armyOne, armyTree);
        }
    }

    @Nested
    class CreationCommanderTest{

        @Test
        @DisplayName("Test if commanderUnit is created as expected")
        public void testCreationOfCommanderUnit(){
            CommanderUnit commanderUnit = new CommanderUnit("Orks",10,10,10);
            assertNotNull(commanderUnit);
            assertEquals("Orks", commanderUnit.getName());
            assertEquals(10, commanderUnit.getHealth());
            assertEquals(10, commanderUnit.getAttack());
            assertEquals(10, commanderUnit.getArmor());
        }

        @Test
        @DisplayName("Test if simple commanderUnit is created as expected")
        public void testCreationOfSimpelCommanderUnit(){
            CommanderUnit commanderUnit = new CommanderUnit("Orks",10);
            assertNotNull(commanderUnit);
            assertEquals("Orks", commanderUnit.getName());
            assertEquals(10, commanderUnit.getHealth());
            assertEquals(25, commanderUnit.getAttack());
            assertEquals(15, commanderUnit.getArmor());
        }
    }

    @Nested
    class CreationinfantryTest{

        @Test
        @DisplayName("Test if infantry unit is crated as expected")
        public void testCreationOfInfantryUnit(){
            InfantryUnit infantryUnit = new InfantryUnit("Orks",10,10,10);
            assertNotNull(infantryUnit);
            assertEquals("Orks", infantryUnit.getName());
            assertEquals(10, infantryUnit.getHealth());
            assertEquals(10, infantryUnit.getAttack());
            assertEquals(10, infantryUnit.getArmor());

        }
        @Test
        @DisplayName("Test if simple infantry unit is created as expected")
        public void testCreationOfSimpelInfantryUnit(){
            InfantryUnit infantryUnit = new InfantryUnit("Orks",10);
            assertNotNull(infantryUnit);
            assertEquals("Orks", infantryUnit.getName());
            assertEquals(10, infantryUnit.getHealth());
            assertEquals(15, infantryUnit.getAttack());
            assertEquals(10, infantryUnit.getArmor());

        }
    }

    @Nested
    class CreatonRangedTest{
        @Test
        @DisplayName("Test if ranged unit is created as expected")
        public void testCreationOfRangedUnit(){
            RangedUnit rangedUnit = new RangedUnit("Orks",10,10,10);
            assertNotNull(rangedUnit);
            assertEquals("Orks", rangedUnit.getName());
            assertEquals(10, rangedUnit.getHealth());
            assertEquals(10, rangedUnit.getAttack());
            assertEquals(10, rangedUnit.getArmor());
        }

        @Test
        @DisplayName("Test if simple ranged unit is crated as expected")
        public void testCreationOfSimpelRangedUnit(){
            RangedUnit rangedUnit = new RangedUnit("Orks", 10);
            assertNotNull(rangedUnit);
            assertEquals("Orks", rangedUnit.getName());
            assertEquals(10, rangedUnit.getHealth());
            assertEquals(15, rangedUnit.getAttack());
            assertEquals(8, rangedUnit.getArmor());
        }
    }

    @Nested
    class CreationCavalryTest{
        @Test
        @DisplayName("Test if cavalry unit is created as expected")
        public void testCreationOfCavaltyUnit(){
            CavalryUnit CavalryUnit = new CavalryUnit("Orks",10,10,10);
            assertNotNull(CavalryUnit);
            assertEquals("Orks", CavalryUnit.getName());
            assertEquals(10, CavalryUnit.getHealth());
            assertEquals(10, CavalryUnit.getAttack());
            assertEquals(10, CavalryUnit.getArmor());
        }

        @Test
        @DisplayName("Test if simple cavalry unit is created as expected")
        public void testCreationOfSimpelCavaltyUnit(){
            CavalryUnit CavalryUnit = new CavalryUnit("Orks",10);
            assertNotNull(CavalryUnit);
            assertEquals("Orks", CavalryUnit.getName());
            assertEquals(10, CavalryUnit.getHealth());
            assertEquals(20, CavalryUnit.getAttack());
            assertEquals(12, CavalryUnit.getArmor());
        }
    }

    @Nested
    class FalseInputTest{

        @Test
        @DisplayName("Test if cration of of unit with negative health throw new IllegalArgumentExcetion")
        public void testIlligaleArgumentCreationOfCavaltyUnitHealth(){

            try {
                new CommanderUnit("Orks",-100);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit can not have a negative health value" );
            }
            assertThrows(IllegalArgumentException.class ,() -> {
                new CavalryUnit("Orks",-100,10,10);
            });

        }

        @Test
        @DisplayName("Test if cration of of unit with no name throw new IllegalArgumentExcetion")
        public void testIlligalArgymentCrationCommanderUnitName(){

            try {
                new CommanderUnit("",10);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit must have a name, i can not be black");
            }

            assertThrows(IllegalArgumentException.class, () -> {
               new CommanderUnit("", 10,10,10);
            });
        }

        @Test
        @DisplayName("Test if cration of of unit with negative attack throw new IllegalArgumentExcetion")
        public void testIlligalArgymentCrationRangedUnitAttack(){

            try {
                new RangedUnit("Orks",10,-100,10);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(),"The unit can not have a negative attack value");
            }

            assertThrows(IllegalArgumentException.class, () -> {
                new RangedUnit("Orks", 10,-100,10);
            });
        }

        @Test
        @DisplayName("Test if cration of of unit with negative armor throw new IllegalArgumentExcetion")
        public void testIlligalArgymentCrationInfantryUnitArmor(){
            try {
                new InfantryUnit("Orks",10,10,-100);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit can not have a negative armor value");
            }
            assertThrows(IllegalArgumentException.class, () -> {
                new InfantryUnit("Orks", 10,10,-100);
            });
        }

        @Test
        public void testIlligalArgymentCrationCommanderUnitAccuracy(){
            try {
                new CavalryUnit("orks", 10,10,10, 1.5,0.5);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit's accuracy cannot be less than 25 percent or greater than 100 percent");
            }
            assertThrows(IllegalArgumentException.class, ()-> {
                new CavalryUnit("orks", 10,10,10, 1.5,0.5);
            });

            //test if accuacy is below 25%
            try {
                new CavalryUnit("orks", 10,10,10, -0.2,0.5);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit's accuracy cannot be less than 25 percent or greater than 100 percent");
            }
            assertThrows(IllegalArgumentException.class, ()-> {
                new CavalryUnit("orks", 10,10,10, -0.2,0.5);
            });
        }

        @Test
        public void testIlligalArgymentCrationCommanderUnitCounterattack(){
            try {
                new CavalryUnit("orks", 10,10,10, 0.5,1.5);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit's counterattack cannot be less than 25 percent or greater than 100 percent");
            }
            assertThrows(IllegalArgumentException.class, ()-> {
                new CavalryUnit("orks", 10,10,10, 0.5,1.5);
            });
            //test if counerattack is below zero
            try {
                new CavalryUnit("orks", 10,10,10, 0.5,-0.4);
                fail();
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "The unit's counterattack cannot be less than 25 percent or greater than 100 percent");
            }
            assertThrows(IllegalArgumentException.class, ()-> {
                new CavalryUnit("orks", 10,10,10, 0.5,-0.4);
            });

        }
    }
}
