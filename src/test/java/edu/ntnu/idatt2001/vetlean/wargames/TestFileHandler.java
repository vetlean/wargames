package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.filehandler.ArmyFileHandler;
import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TestFileHandler {

    public Army makTestDataArmyHuman(){

        ArrayList<Unit> units = new ArrayList<>();

        for(int i =0; i < 500; i++){
            InfantryUnit infantryUnit = new InfantryUnit("Footman "+i, 100);
            units.add(infantryUnit);
        }

        for(int i =0; i < 100; i++){
            CavalryUnit cavalryUnit= new CavalryUnit("Knight "+i, 100);
            units.add(cavalryUnit);
        }

        for (int i = 0; i < 200; i++){
            RangedUnit rangedUnit = new RangedUnit("Archer "+i,100);
            units.add(rangedUnit);
        }

        for(int i = 0; i<10; i++){
            CommanderUnit commanderUnit = new CommanderUnit("Mountain King", 180);
            units.add(commanderUnit);
        }

        return new Army("Humnas", units);
    }

    @Test
    @DisplayName("test if write to file makes a file")
    void testWriteToFile(){

        Army army = makTestDataArmyHuman();
        File file = new File("src/test/resources/army.csv");
        ArmyFileHandler fileHandler = new ArmyFileHandler();

        try {
            fileHandler.writeArmy(army,file);
        }catch (IOException e){
            fail();
        }
    }

    @Test
    @DisplayName("test write to file if end og file name is not .csv")
    public void testWriteToFileIfEndOfFileIsNotCsv(){

        Army army = makTestDataArmyHuman();
        File file = new File("src/test/resources/army.doc");
        ArmyFileHandler fileHandler = new ArmyFileHandler();

        try {
            fileHandler.writeArmy(army,file);
        }catch (IOException e){
           assertEquals(e.getMessage(),"Unsupported file format. Only .csv-files are supported");
        }
        assertThrows(IOException.class, ()->{
            fileHandler.writeArmy(army,file);
        });
    }

    @Test
    @DisplayName("test write to file army when army is null")
    public void testWriteToFileIfArmyIsNull(){

        Army army = null;
        File file = new File("src/test/resources/army.csv");
        ArmyFileHandler fileHandler = new ArmyFileHandler();

        try {
            fileHandler.writeArmy(army,file);
        }catch (NullPointerException | IOException e){
            assertEquals(e.getMessage(),"Army can not be null");
        }
        assertThrows(NullPointerException.class, ()->{
            fileHandler.writeArmy(army,file);
        });
    }

    @Test
    @DisplayName("test army if read is making a army")
    public void TestArmyReader(){

        Army army = null;

        ArmyFileHandler armyFileHandler = new ArmyFileHandler();
        try {
            army = armyFileHandler.readArmy(new File("src/test/resources/army.csv"));
        }catch (IOException e){
            e.printStackTrace();
        }

        assertNotNull(army);
    }

    @Test
    @DisplayName("test reading file with where name and health is switched")
    public void testReadFileWithNameAndHealtSwitched(){
        ArmyFileHandler armyFileHandler = new ArmyFileHandler();

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNameAndHealtSwitched.csv"));
        }catch(NumberFormatException e){
            assertEquals(e.getMessage(),"Wrong format, health should be placed last, as number 3");
        }catch (IOException e){
            fail();
        }
        assertThrows(NumberFormatException.class, ()->{
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNameAndHealtSwitched.csv"));
        });
    }

    @Test
    @DisplayName("test reading file with no name of the army")
    public void testReafFileWithNoArmyNameOnTop(){
        ArmyFileHandler armyFileHandler = new ArmyFileHandler();

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNoArmyNameOnTop.csv"));
            fail();
        }catch (IOException e){
            assertEquals(e.getMessage(),"Wrong format, the army must have a name");
        }
        assertThrows(IOException.class, ()-> {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNoArmyNameOnTop.csv"));
        });
    }

    @Test
    @DisplayName("test reading file with no separation with comma")
    public void testReadFileWithNoCommas(){
        ArmyFileHandler armyFileHandler = new ArmyFileHandler();

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNoComma.csv"));
        } catch (IOException e) {
            assertEquals(e.getMessage(),"Wrong format, the csv file is not structured properly");
        }
        assertThrows(IOException.class, ()-> {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/armyNoComma.csv"));
        });
    }

    @Test
    @DisplayName("test reading a file that is not .csv")
    public void testReadFileNotAsCsv(){
        ArmyFileHandler armyFileHandler = new ArmyFileHandler();

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/IDATT1002 Project-assignment-2022_trd (1).pdf"));
        }catch (IOException e){
            assertEquals(e.getMessage(), "Unsupported file format. Only .csv-files are supported");
        }
        assertThrows(IOException.class, ()-> {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/IDATT1002 Project-assignment-2022_trd (1).pdf"));
        });

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/Template - Main Report - v2022 (1).docx"));
        }catch (IOException e){
            assertEquals(e.getMessage(), "Unsupported file format. Only .csv-files are supported");
        }
        assertThrows(IOException.class, ()-> {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/Template - Main Report - v2022 (1).docx"));
        });
    }

    @Test
    @DisplayName("test reading an empty csv file")
    public void testReadingEmptyFile(){
        ArmyFileHandler armyFileHandler = new ArmyFileHandler();

        try {
            Army army = armyFileHandler.readArmy(new File("src/test/resources/emptyArmy.csv"));
        }catch (NullPointerException | IOException e){
            assertEquals(e.getMessage(), "The file is empty, it contains no data.");
        }
        assertThrows(NullPointerException.class, ()->{
           Army army = armyFileHandler.readArmy(new File("src/test/resources/emptyArmy.csv"));
        });
    }
}
