package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.units.CavalryUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.InfantryUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.RangedUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AttackTest {

    @Nested
    class TestAttackForCavalry{

        @Test
        @DisplayName("Test if cavalryUnit attack bonus decreases after first attack")
        public void testAttacingBonusForCavalryUnitFiveTimes(){
            CavalryUnit attacingCavalryUnit = new CavalryUnit("Orks",10,10,10);
            CavalryUnit defenceCavalryUnit= new CavalryUnit("Orks",100,10,9);

            //The terrain Hill does not affect Cavalry Unit attack
            // but decreases its defence
            assertEquals(6, attacingCavalryUnit.getAttackBonus(Terrain.HILL));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.HILL);
            assertEquals(4, attacingCavalryUnit.getAttackBonus(Terrain.HILL));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.HILL);
            assertEquals(3, attacingCavalryUnit.getAttackBonus(Terrain.HILL));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.HILL);
            assertEquals(2, attacingCavalryUnit.getAttackBonus(Terrain.HILL));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.HILL);
            assertEquals(0, attacingCavalryUnit.getAttackBonus(Terrain.HILL));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.HILL);
            assertEquals(0, attacingCavalryUnit.getAttackBonus(Terrain.HILL));

            assertEquals(85, defenceCavalryUnit.getHealth());
        }

        @Test
        @DisplayName("Test if cavalryUnit testAttack bonus decreases after first testAttack in plains terrain")
        public void testAttacingBonusForCavalryUnitFiveTimesInPlanesTerrain(){
            CavalryUnit attacingCavalryUnit = new CavalryUnit("Orks",10,10,10);
            CavalryUnit defenceCavalryUnit= new CavalryUnit("Orks",100,10,9);

            //The terrain Plains increases Cavalry Units attack
            assertEquals(10, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.PLAINS);
            assertEquals(8, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.PLAINS);
            assertEquals(7, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.PLAINS);
            assertEquals(6, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.PLAINS);
            assertEquals(4, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.PLAINS);
            assertEquals(4, attacingCavalryUnit.getAttackBonus(Terrain.PLAINS));

            assertEquals(65, defenceCavalryUnit.getHealth());
        }

        @Test
        @DisplayName("Test if cavalryUnit testAttack bonus and defence decreases in forest terrain")
        public void testAttacingBonusForCavalryUnitFiveTimesInForestTerrain(){
            CavalryUnit attacingCavalryUnit = new CavalryUnit("Orks",10,10,10);
            CavalryUnit defenceCavalryUnit = new CavalryUnit("Orks",100,10,0);

            //The terrain Forrest does not affect Cavalry Unit attack
            // but decreases its defence
            assertEquals(6, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.FOREST);
            assertEquals(4, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.FOREST);
            assertEquals(3, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.FOREST);
            assertEquals(2, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.FOREST);
            assertEquals(0, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));
            attacingCavalryUnit.testAttack(defenceCavalryUnit,Terrain.FOREST);
            assertEquals(0, attacingCavalryUnit.getAttackBonus(Terrain.FOREST));

            assertEquals(35, defenceCavalryUnit.getHealth());
        }

    }

    @Nested
    class TestAttackForInfantry{

        @Test
        @DisplayName("Test Attack for InfantryUnit 5 times")
        public void testAttackForInfantryUnitFiveTimes(){
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,10,10);
            InfantryUnit defenceinfantryUnit= new InfantryUnit("Orks",100,10,0);

            //The terrain Forrest does not affect Infantry Unit
            for(int i = 0; i < 5; i++){
                attacingInfantryUnit.testAttack(defenceinfantryUnit, Terrain.HILL);
            }

            assertEquals(45, defenceinfantryUnit.getHealth());
        }



        @Test
        @DisplayName("Test if infantryUnit dies")
        public void testAttackForInfantryUnitToDeath(){
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,10,10);
            InfantryUnit defenceInfantryUnit= new InfantryUnit("Orks",100,10,0);

            //The terrain Hill does not affect Infantry Unit
            for(int i = 0; i < 12; i++){
                attacingInfantryUnit.testAttack(defenceInfantryUnit, Terrain.HILL);
            }
            assertEquals(0, defenceInfantryUnit.getHealth());
        }

        @Test
        @DisplayName("Test if unit takes damage if armor is grater then testAttack, it shall not")
        public void testAttackForInfantryUnitButLowerAttackThanArmor(){
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,10,10);
            InfantryUnit defenceInfantryUnit= new InfantryUnit("Orks",100,10,100);

            //The terrain Hill does not affect Infantry Unit
            for(int i = 0; i < 12; i++){
                attacingInfantryUnit.testAttack(defenceInfantryUnit, Terrain.HILL);
            }
            assertEquals(100, defenceInfantryUnit.getHealth());
        }

        @Test
        @DisplayName("Test if armor takes damage from testAttack as expected")
        public void testArmorForInfantryUnitTenTimes(){
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,10,10);
            InfantryUnit defenceInfantryUnit= new InfantryUnit("Orks",100,10,5);

            //The terrain Hill does not affect Infantry Unit
            for(int i = 0; i < 10; i++){
                attacingInfantryUnit.testAttack(defenceInfantryUnit, Terrain.HILL);
            }
            assertEquals(40, defenceInfantryUnit.getHealth());
        }

        @Test
        @DisplayName("Test if armo bonus taks damage as expected")
        public void testArmorBonus(){
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,0,10);
            InfantryUnit defenceInfantryUnit= new InfantryUnit("Orks",100,10,0);

            for(int i = 0; i < 10; i++){
                attacingInfantryUnit.testAttack(defenceInfantryUnit, Terrain.HILL);
            }
            assertEquals(90, defenceInfantryUnit.getHealth() );
        }

        @Test
        @DisplayName("Test if testAttack bouns give damage as expected")
        public void testAttackBonus(){
            //The terrain Hill does not affect Infantry Unit
            //The terrain Hill does not affect Cavalry Unit
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,0,10);
            CavalryUnit defenceCavalryUnit= new CavalryUnit("Orks",100,10,0);

            for(int i = 0; i < 10; i++){
                attacingInfantryUnit.testAttack(defenceCavalryUnit, Terrain.HILL);
            }
            assertEquals(90, defenceCavalryUnit.getHealth() );
        }

        @Test
        @DisplayName("Test Attack for InfantryUnit 5 times in forest terrain")
        public void testAttackForInfantryUnitFiveTimesInForestTerrain(){
            //The terrain Forest increases Infantry Unit attack
            //The terrain Forest decreases Cavalry unit defence
            InfantryUnit attacingInfantryUnit = new InfantryUnit("Orks",10,10,10);
            CavalryUnit defenceCavalryUnit= new CavalryUnit("Orks",100,10,0);

            for(int i = 0; i < 5; i++){
                attacingInfantryUnit.testAttack(defenceCavalryUnit, Terrain.FOREST);
            }

            assertEquals(30, defenceCavalryUnit.getHealth());
        }

        @Test
        @DisplayName("Test Defence for InfantryUnit 5 times in forest terrain")
        public void testDefenceForInfantryUnitFiveTimesInForestTerrain(){
            //The terrain Forest decreases Ranged Unit attack
            //The terrain Forest increases Infantry unit defence
            RangedUnit attackingRangedUnit = new RangedUnit("Orks",10,10,10);
            InfantryUnit defenceInfantryUnit= new InfantryUnit("Orks",100,10,0);
            for(int i = 0; i < 5; i++){
                attackingRangedUnit.testAttack(defenceInfantryUnit, Terrain.FOREST);
            }

            assertEquals(60, defenceInfantryUnit.getHealth());
        }
    }

    @Nested
    class TestAttackForRanged{
        @Test
        @DisplayName("Test if Ranged armor bonus decreses after first testAttack")
        public void testDefenceBonusForRangedUnitFiveTimes(){
            //The terrain Plains increases Ranged Unit attack
            RangedUnit attacingRangedunit = new RangedUnit("Orks",10,7,10);
            RangedUnit defenceRangedUnit= new RangedUnit("Orks",100,10,0);

            assertEquals(6, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(4, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(3, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(2, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(1, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(1, defenceRangedUnit.getResistBonus(Terrain.PLAINS));
            attacingRangedunit.testAttack(defenceRangedUnit,Terrain.PLAINS);

            assertEquals(57, defenceRangedUnit.getHealth());
        }

        @Test
        @DisplayName("test Attcak Bonus For RangedUnit In Hill Terrain 5 times")
        public void testAttcakBonusForRangedUnitInHillTerrainFiveTimes(){
            //The terrain Hill does not affect Ranged Unit attack
            RangedUnit attackingRangingUnit = new RangedUnit("Orks",10,5,10);
            InfantryUnit defenceInfantryUnit = new InfantryUnit("Orks",100,10,0);

            for(int i = 0; i<5; i++){
                assertEquals(5, attackingRangingUnit.getAttackBonus(Terrain.HILL));
                attackingRangingUnit.testAttack(defenceInfantryUnit,Terrain.HILL);
            }

            assertEquals(55, defenceInfantryUnit.getHealth());
        }

        @Test
        @DisplayName("test Attcak Bonus For RangedUnit In Forest terrain 5 times")
        public void testAttcakBonusForRangedUnitInForestTerrainFiveTimes(){

            //The terrain Forest decreases Ranged Unit attack
            //The terrain Forest increases Infantry Unit defence
            RangedUnit attackingRangingUnit = new RangedUnit("Orks",10,5,10);
            InfantryUnit defenceInfantryUnit = new InfantryUnit("Orks",100,10,0);

            for(int i = 0; i < 5; i++){
                assertEquals(1, attackingRangingUnit.getAttackBonus(Terrain.FOREST));
                attackingRangingUnit.testAttack(defenceInfantryUnit,Terrain.FOREST);
            }

            assertEquals(85, defenceInfantryUnit.getHealth());
        }
    }

    @Nested
    class TestAccuracyParameterWithAttack{

        public boolean hit(double simulatedOutcomeRandom){
            double accuracy = 0.90;
            return simulatedOutcomeRandom < accuracy;
        }

        @Test
        public void testSimularStructuredHitMethod(){
            assertFalse(hit(0.93342));
            assertTrue(hit(0.45223));
        }

        @Test
        public void testAttackWithZeroPercentAccuracy(){
            //The terrain Hill does not affect Cavalry Unit attack
            // but decreases its defence
            CavalryUnit attacking = new CavalryUnit("cavalryUnit",100,10,10,0, 0);
            CavalryUnit defending = new CavalryUnit("cavalryUnit",100,10,9,0,0);

            assertEquals(6, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(4, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(3, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(2, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(0, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(0, attacking.getAttackBonus(Terrain.HILL));

            assertEquals(100, defending.getHealth());
        }

        @Test
        public void testAttackWithOneHundredPercentCounterAttack(){
            CavalryUnit defending = new CavalryUnit("cavalryUnit",100,10,10,1, 1);
            CavalryUnit attacking = new CavalryUnit("cavalryUnit",100,10,9,1,0);

            //The terrain Hill does not affect Cavalry Unit attack
            // but decreases its defence
            assertEquals(6, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(4, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(3, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(2, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(0, attacking.getAttackBonus(Terrain.HILL));
            attacking.attack(defending,Terrain.HILL);
            assertEquals(0, attacking.getAttackBonus(Terrain.HILL));

            assertEquals(85, attacking.getHealth());
        }
    }
}
