package edu.ntnu.idatt2001.vetlean.wargames;

import edu.ntnu.idatt2001.vetlean.wargames.models.Army;
import edu.ntnu.idatt2001.vetlean.wargames.units.CavalryUnit;
import edu.ntnu.idatt2001.vetlean.wargames.units.Unit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


import java.util.ArrayList;


public class GetRandomTest {

    @Test
    @DisplayName("Test if army can get random unit")
    public void testFindRandomUnit(){

        ArrayList<Unit> units = new ArrayList<>();
        CavalryUnit ork1 = new CavalryUnit("Ork1",10);
        CavalryUnit ork2 = new CavalryUnit("Ork2",10);
        CavalryUnit ork3 = new CavalryUnit("Ork3",10);
        CavalryUnit ork4 = new CavalryUnit("Ork4",10);
        CavalryUnit ork5 = new CavalryUnit("Ork5",10);
        units.add(ork1);
        units.add(ork2);
        units.add(ork3);
        units.add(ork4);
        units.add(ork5);

        Army army = new Army("Army1", units);

        assertNotNull(army.getRandom());
    }

    @Test
    @DisplayName("Test if random workes with only one unit")
    public void testFindRandomUnitOnlyOneUnit(){

        ArrayList<Unit> units = new ArrayList<>();
        CavalryUnit ork1 = new CavalryUnit("Ork1",10);
        units.add(ork1);

        Army army = new Army("Army1", units);

        assertNotNull(army.getRandom());
    }

    @Test
    @DisplayName("Test if random throws IndexOutOfBoundsExeption if Army is out of units")
    public void testFindRandomInEmptyArmy(){

        ArrayList<Unit> units = new ArrayList<>();

        Army army = new Army("Army1", units);

        assertThrows(IndexOutOfBoundsException.class, army::getRandom);
    }
}
